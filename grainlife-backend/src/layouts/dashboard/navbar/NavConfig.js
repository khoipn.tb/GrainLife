// routes
import { PATH_DASHBOARD } from '../../../routes/paths';
import SvgIconStyle from '../../../components/SvgIconStyle';

// ----------------------------------------------------------------------

const getIcon = (name) => <SvgIconStyle src={`/icons/${name}.svg`} sx={{ width: 1, height: 1 }} />;

const ICONS = {
  blog: getIcon('ic_blog'),
  cart: getIcon('ic_cart'),
  chat: getIcon('ic_chat'),
  mail: getIcon('ic_mail'),
  user: getIcon('ic_user'),
  kanban: getIcon('ic_kanban'),
  banking: getIcon('ic_banking'),
  calendar: getIcon('ic_calendar'),
  ecommerce: getIcon('ic_ecommerce'),
  analytics: getIcon('ic_analytics'),
  dashboard: getIcon('ic_dashboard'),
  booking: getIcon('ic_booking'),
  category: getIcon('ic_category'),
};

const navConfig = [
  // GENERAL
  // ----------------------------------------------------------------------
  {
    subheader: 'Chung',
    items: [
      { title: 'Tổng quan', path: PATH_DASHBOARD.overview.root, icon:  ICONS.dashboard },
    ],
  },

  // MANAGEMENT
  // ----------------------------------------------------------------------
  {
    subheader: 'Quản lý',
    items: [
      // MANAGEMENT : USER
      {
        title: 'Người dùng',
        path: PATH_DASHBOARD.user.root,
        icon: ICONS.user,
        children: [
          // { title: 'profile', path: PATH_DASHBOARD.user.profile },
          // { title: 'cards', path: PATH_DASHBOARD.user.cards },
          { title: 'Danh sách', path: PATH_DASHBOARD.user.list },
          { title: 'Thêm mới', path: PATH_DASHBOARD.user.new },
          // { title: 'edit', path: PATH_DASHBOARD.user.editById },
          // { title: 'account', path: PATH_DASHBOARD.user.account },
        ],
      },

      // MANAGEMENT : CATEGORY
      {
        title: 'Danh mục',
        path: PATH_DASHBOARD.category.root,
        icon: ICONS.category,
        children: [
          { title: 'Danh sách', path: PATH_DASHBOARD.category.list },
          { title: 'Thêm mới', path: PATH_DASHBOARD.category.new },
        ],
      },

      // MANAGEMENT : PRODUCT
      {
        title: 'Sản phẩm',
        path: PATH_DASHBOARD.product.root,
        icon: ICONS.cart,
        children: [
          { title: 'Danh sách', path: PATH_DASHBOARD.product.list },
          { title: 'Thêm mới', path: PATH_DASHBOARD.product.new },
        ],
      },

      // MANAGEMENT : ORDER
      {
        title: 'Đơn hàng',
        path: PATH_DASHBOARD.order.root,
        icon: ICONS.cart,
        children: [
          { title: 'Danh sách', path: PATH_DASHBOARD.order.list },
        ],
      },

      // MANAGEMENT : BLOG
      {
        title: 'Tin tức',
        path: PATH_DASHBOARD.blog.root,
        icon: ICONS.blog,
        children: [
          { title: 'Danh sách', path: PATH_DASHBOARD.blog.list },
          { title: 'Thêm mới', path: PATH_DASHBOARD.blog.newPost },
        ],
      },
    ],
  },
];

export default navConfig;
