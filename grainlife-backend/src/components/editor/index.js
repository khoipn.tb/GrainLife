import PropTypes from 'prop-types';
import ReactQuill, {Quill} from 'react-quill';
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.core.css";
// @mui
import {styled} from '@mui/material/styles';
import {Box} from '@mui/material';
//
import {useMemo, useRef} from "react";
import ImageResize from 'quill-image-resize-module-react';
import ImageUploader from "quill-image-uploader";
import EditorToolbar, {formats, redoChange, undoChange} from './EditorToolbar';
import axios from "../../utils/axios";

Quill.register('modules/imageResize', ImageResize);
Quill.register("modules/imageUploader", ImageUploader);

const ATTRIBUTES = [
	'alt',
	'height',
	'width',
	'style'
];

const ParchmentEmbed = Quill.import('blots/block/embed');
class ImageWithStyle extends ParchmentEmbed {
	static create(value) {
		const node = super.create(value);
		if (typeof value === 'string') {
			node.setAttribute('src', this.sanitize(value));
		}
		return node;
	}

	static formats(domNode) {
		// debugger;
		return ATTRIBUTES.reduce((formats, attribute) => {
			if (domNode.hasAttribute(attribute)) {
				formats[attribute] = domNode.getAttribute(attribute);
			}
			return formats;
		}, {});
	}

	static match(url) {
		return /\.(jpe?g|gif|png)$/.test(url) || /^data:image\/.+;base64/.test(url);
	}

	static sanitize(url) {
		return url;
		// return sanitize(url, ['http', 'https', 'data']) ? url : '//:0';
	}


	static value(domNode) {
		return domNode.getAttribute('src');
	}

	format(name, value) {
		if (ATTRIBUTES.indexOf(name) > -1) {
			if (value) {
				this.domNode.setAttribute(name, value);
			} else {
				this.domNode.removeAttribute(name);
			}
		} else {
			super.format(name, value);
		}
	}
}
ImageWithStyle.blotName = 'imagewithstyle';
ImageWithStyle.tagName = 'IMG';
Quill.register(ImageWithStyle, true);

// ----------------------------------------------------------------------

const RootStyle = styled(Box)(({theme}) => ({
	borderRadius: theme.shape.borderRadius,
	border: `solid 1px ${theme.palette.grey[500_32]}`,
	'& .ql-container.ql-snow': {
		borderColor: 'transparent',
		...theme.typography.body1,
		fontFamily: theme.typography.fontFamily,
	},
	'& .ql-editor': {
		minHeight: 200,
		'&.ql-blank::before': {
			fontStyle: 'normal',
			color: theme.palette.text.disabled,
		},
		'& pre.ql-syntax': {
			...theme.typography.body2,
			padding: theme.spacing(2),
			borderRadius: theme.shape.borderRadius,
			backgroundColor: theme.palette.grey[900],
		},
	},
}));

// ----------------------------------------------------------------------

Editor.propTypes = {
	id: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	error: PropTypes.bool,
	helperText: PropTypes.node,
	simple: PropTypes.bool,
	sx: PropTypes.object,
};

export default function Editor({
																 id = 'minimal-quill',
																 error,
																 value,
																 onChange,
																 simple = false,
																 helperText,
																 sx,
																 ...other
															 }) {
	const editorRef = useRef(null);
	// const imageHandler = () => {
	// 	const input = document.createElement("input");
	// 	input.setAttribute("type", "file");
	// 	input.setAttribute("accept", "image/*");
	// 	input.click();
	//
	// 	input.onchange = async () => {
	// 		const file = input.files[0];
	//
	// 		// file type is only image.
	// 		if (/^image\//.test(file.type)) {
	// 			try {
	// 				const formData = new FormData();
	// 				formData.append("embed", file);
	// 				const response = await axios.post('/upload', formData, {
	// 					headers: {
	// 						'Content-Type': 'multipart/form-data'
	// 					}
	// 				});
	// 				const range = editorRef.current.getEditorSelection();
	// 				editorRef.current.getEditor().insertEmbed(range.index, "image", response.data.url);
	// 			} catch (e) {
	// 				console.log(e);
	// 			}
	// 		} else {
	// 			console.warn("You could only upload images.");
	// 		}
	// 	};
	// };

	const modules = useMemo(() => ({
		toolbar: {
			container: `#${id}`,
			handlers: {
				undo: undoChange,
				redo: redoChange,
				// image: imageHandler,
			},
		},
		history: {
			delay: 500,
			maxStack: 100,
			userOnly: true,
		},
		syntax: true,
		clipboard: {
			matchVisual: false,
		},
		imageResize: {
			parchment: Quill.import('parchment'),
			modules: ['Resize', 'DisplaySize', 'Toolbar']
		},
		imageUploader: {
			// eslint-disable-next-line no-async-promise-executor
			upload: file => new Promise(async (resolve, reject) => {
				try {
					const formData = new FormData();
					formData.append("embed", file);
					const response = await axios.post('/upload', formData, {
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					});
					resolve(response.data.url);
				} catch (e) {
					// eslint-disable-next-line prefer-promise-reject-errors
					reject("Có lỗi khi tải ảnh lên");
					console.log(e);
				}
			})
		}
	}), [id]);

	return (
		<div>
			<RootStyle
				sx={{
					...(error && {
						border: (theme) => `solid 1px ${theme.palette.error.main}`,
					}),
					...sx,
				}}
			>
				<EditorToolbar id={id} isSimple={simple}/>
				<ReactQuill
					ref={editorRef}
					value={value}
					onChange={onChange}
					modules={modules}
					formats={formats}
					theme='snow'
					placeholder="Hãy viết một cái gì đó ..."
					{...other}
				/>
			</RootStyle>

			{helperText && helperText}
		</div>
	);
}
