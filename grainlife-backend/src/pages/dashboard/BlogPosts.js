import {useState, useEffect, useRef} from 'react';
// @mui
import { useTheme } from '@mui/material/styles';
import {
  Card,
  Table,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button,
} from '@mui/material';
// utils
import {useSnackbar} from "notistack";
import { fDate } from '../../utils/formatTime';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import Label from '../../components/Label';
import Image from '../../components/Image';
import Scrollbar from '../../components/Scrollbar';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// sections
import {
  ProductListHead,
} from '../../sections/@dashboard/e-commerce/product-list';
import axios from "../../utils/axios";
import BlogMoreMenu from "../../sections/@dashboard/blog/BlogMoreMenu";

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'title', label: 'Tiêu đề', alignRight: false },
  { id: 'createdAt', label: 'Ngày tạo', alignRight: false },
  { id: 'inventoryType', label: 'Trạng thái', alignRight: false },
  { id: 'enableComment', label: 'Cho phép bình luận', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

export default function BlogPosts() {
  const { themeStretch } = useSettings();
  const theme = useTheme();
  const { enqueueSnackbar } = useSnackbar();

  const [blogList, setBlogList] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [totalPage, setTotalPage] = useState(1);
  const [isOpenConfirmDeleteDialog, setIsOpenConfirmDeleteDialog] = useState(false);
  const selectingItemToDelete = useRef();

  useEffect(() => {
    const getProductList = async () => {
      try {
        const response = await axios.get(`/blogs?limit=${rowsPerPage}&page=${page + 1}`);
        setBlogList(response.data.data);
        setTotalPage(response.data.totalPages);
      } catch (e) {
        console.log(e);
      }
    };
    getProductList();
  }, [page, rowsPerPage]);

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleOpenConfirmDeleteDialog = () => {
    setIsOpenConfirmDeleteDialog(true);
  };

  const handleCloseConfirmDeleteDialog = () => {
    setIsOpenConfirmDeleteDialog(false);
  };

  const handleAgreeDeleteDialog = async () => {
    const productId = selectingItemToDelete.current;
    try {
      await axios.delete(`/blogs/${productId}`);
      const deleteProduct = blogList.filter((product) => product._id !== productId);
      setBlogList(deleteProduct);
      enqueueSnackbar(`Xóa thành công`);
    } catch (e) {
      enqueueSnackbar(`Đã xảy ra sự cố: ${e?.message}`, {variant: 'error'});
      console.log(e);
    }
    handleCloseConfirmDeleteDialog();
  };

  const handleDeleteProduct = (productId) => {
    selectingItemToDelete.current = productId;
    handleOpenConfirmDeleteDialog();
  };

  return (
    <Page title="Danh sách tin tức">
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading="Danh sách tin tức"
          links={[
            { name: 'Bảng điều khiển', href: PATH_DASHBOARD.root },
            {
              name: 'Tin tức',
              href: PATH_DASHBOARD.product.root,
            },
            { name: 'Danh sách tin tức' },
          ]}
        />

        <Card>
          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <ProductListHead
                  headLabel={TABLE_HEAD}
                />
                <TableBody>
                  {blogList.map((row) => {
                    const { _id, title, cover, enableComment, createdAt, isActive } = row;

                    return (
                      <TableRow
                        hover
                        key={_id}
                        tabIndex={-1}
                        role="checkbox"
                      >
                        <TableCell sx={{ display: 'flex', alignItems: 'center' }}>
                          <Image
                            disabledEffect
                            alt={title}
                            src={cover}
                            sx={{ borderRadius: 1.5, width: 64, height: 64, mr: 2 }}
                          />
                          <Typography variant="subtitle2" noWrap>
                            {title.length > 50 ?
                              `${title.substring(0, 50)}...` : title
                            }
                          </Typography>
                        </TableCell>
                        <TableCell style={{ minWidth: 160 }}>{fDate(createdAt)}</TableCell>
                        <TableCell style={{ minWidth: 160 }}>
                          <Label
                            variant={theme.palette.mode === 'light' ? 'ghost' : 'filled'}
                            color={
                              isActive ? 'success' : 'warning'
                            }
                          >
                            {isActive ? 'Hiển thị' : 'Ẩn'}
                          </Label>
                        </TableCell>
                        <TableCell style={{ minWidth: 160 }}>
                          <Label
                            variant={theme.palette.mode === 'light' ? 'ghost' : 'filled'}
                            color={
                              enableComment ? 'success' : 'warning'
                            }
                          >
                            {enableComment ? 'Cho phép' : 'Chặn'}
                          </Label>
                        </TableCell>
                        <TableCell align="right">
                          <BlogMoreMenu blogId={_id} onDelete={() => handleDeleteProduct(_id)} />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>

                {/* {isNotFound && ( */}
                {/*  <TableBody> */}
                {/*    <TableRow> */}
                {/*      <TableCell align="center" colSpan={6}> */}
                {/*        <Box sx={{ py: 3 }}> */}
                {/*          <SearchNotFound searchQuery={filterName} /> */}
                {/*        </Box> */}
                {/*      </TableCell> */}
                {/*    </TableRow> */}
                {/*  </TableBody> */}
                {/* )} */}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            labelRowsPerPage="Số hàng trên mỗi trang"
            rowsPerPageOptions={[1, 5, 10, 25]}
            component="div"
            count={totalPage}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={(event, value) => setPage(value)}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      <Dialog
        open={isOpenConfirmDeleteDialog}
        onClose={handleCloseConfirmDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bạn có chắc chắn muốn xóa?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Lưu ý: Sau khi xóa sẽ không thể khôi phục lại.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmDeleteDialog}>Từ chối</Button>
          <Button onClick={handleAgreeDeleteDialog} autoFocus>
            Đồng ý
          </Button>
        </DialogActions>
      </Dialog>
    </Page>
  );
}
