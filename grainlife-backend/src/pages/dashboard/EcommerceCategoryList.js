import { sentenceCase } from 'change-case';
import {useState, useEffect, useRef} from 'react';
// @mui
import { useTheme } from '@mui/material/styles';
import {
  Card,
  Table,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Dialog,
} from '@mui/material';
// redux
import {useSnackbar} from "notistack";
// utils
import { fDate } from '../../utils/formatTime';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import Label from '../../components/Label';
import Scrollbar from '../../components/Scrollbar';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// sections
import {
  ProductListHead,
} from '../../sections/@dashboard/e-commerce/product-list';
import CategoryMoreMenu from "../../sections/@dashboard/e-commerce/category-list/CategoryMoreMenu";
import axios from "../../utils/axios";

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Danh mục', alignRight: false },
  { id: 'createdAt', label: 'Ngày tạo', alignRight: false },
  { id: 'isActive', label: 'Trạng thái', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

export default function EcommerceCategoryList() {
  const { themeStretch } = useSettings();
  const { enqueueSnackbar } = useSnackbar();
  const theme = useTheme();

  const [categoryList, setCategoryList] = useState([]);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [orderBy, setOrderBy] = useState('createdAt');
  const [isOpenConfirmDeleteDialog, setIsOpenConfirmDeleteDialog] = useState(false);
  const selectingItemToDelete = useRef();

  useEffect(() => {
    const getCategories = async () => {
      try {
        const response = await axios.get(`/categories`);
        setCategoryList(response.data.data);
      } catch (e) {
        console.log(e);
      }
    };
    getCategories();
  }, []);

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleOpenConfirmDeleteDialog = () => {
    setIsOpenConfirmDeleteDialog(true);
  };

  const handleCloseConfirmDeleteDialog = () => {
    setIsOpenConfirmDeleteDialog(false);
  };

  const handleAgreeDeleteDialog = async () => {
    const categoryId = selectingItemToDelete.current;
    try {
      await axios.delete(`/categories/${categoryId}`);
      const deleteCategory = categoryList.filter((category) => category._id !== categoryId);
      setSelected([]);
      setCategoryList(deleteCategory);
      enqueueSnackbar(`Xóa thành công`);
    } catch (e) {
      enqueueSnackbar(`Đã xảy ra sự cố: ${e?.message}`, {variant: 'error'});
      console.log(e);
    }
    handleCloseConfirmDeleteDialog();
  };

  const handleDeleteCategory = (categoryId) => {
    selectingItemToDelete.current = categoryId;
    handleOpenConfirmDeleteDialog();
  };


  return (
    <Page title="Danh sách danh mục">
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading="Danh sách danh mục"
          links={[
            { name: 'Bảng điều khiển', href: PATH_DASHBOARD.root },
            {
              name: 'Danh mục',
              href: PATH_DASHBOARD.category.root,
            },
            { name: 'Danh sách danh mục' },
          ]}
        />

        <Card>
          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <ProductListHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  onRequestSort={handleRequestSort}
                />

                <TableBody>
                  {categoryList.map((row) => {
                    const { _id, name, createdAt, isActive } = row;

                    const isItemSelected = selected.indexOf(name) !== -1;

                    return (
                      <TableRow
                        hover
                        key={_id}
                        tabIndex={-1}
                        role="checkbox"
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell sx={{ display: 'flex', alignItems: 'center' }}>
                          <Typography variant="subtitle2" noWrap>
                            {name}
                          </Typography>
                        </TableCell>
                        <TableCell style={{ minWidth: 160 }}>{fDate(createdAt)}</TableCell>
                        <TableCell style={{ minWidth: 160 }}>
                          <Label
                            variant={theme.palette.mode === 'light' ? 'ghost' : 'filled'}
                            color={
                              isActive ? 'success' : 'warning'
                            }
                          >
                            {isActive ? 'Hiển thị' : 'Ẩn'}
                          </Label>
                        </TableCell>
                        <TableCell align="right">
                          <CategoryMoreMenu categoryId={_id} onDelete={() => handleDeleteCategory(_id)} />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={categoryList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={(event, value) => setPage(value)}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      <Dialog
        open={isOpenConfirmDeleteDialog}
        onClose={handleCloseConfirmDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bạn có chắc chắn muốn xóa?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Lưu ý: Sau khi xóa sẽ không thể khôi phục lại.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmDeleteDialog}>Từ chối</Button>
          <Button onClick={handleAgreeDeleteDialog} autoFocus>
            Đồng ý
          </Button>
        </DialogActions>
      </Dialog>
    </Page>
  );
}

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  if (query) {
    return array.filter((_product) => _product.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }

  return stabilizedThis.map((el) => el[0]);
}
