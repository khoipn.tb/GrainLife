// @mui
import {Box, Container, CircularProgress} from '@mui/material';
// routes
import {useLocation, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import { PATH_DASHBOARD } from '../../routes/paths';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import { BlogNewPostForm } from '../../sections/@dashboard/blog';
import axios from "../../utils/axios";

// ----------------------------------------------------------------------

export default function BlogNewPost() {
  const { themeStretch } = useSettings();
  const { pathname } = useLocation();
  const { id } = useParams();
  const isEdit = pathname.includes('edit');
  const [currentBlog, setCurrentBlog] = useState(null);
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    const getBlog = async () => {
      try {
        const response = await axios.get(`/blogs/${id}`);
        setCurrentBlog(response.data.data);
      } catch (e) {
        console.log(e);
      }
    };

    const prepare = async () => {
      try {
        if (isEdit) {
          await getBlog();
        }
        setIsReady(true);
      } catch (e) {
        console.log(e)
      }
    };

    prepare()
  }, [id, isEdit]);

  return (
    <Page title={!isEdit ? 'Thêm mới bài viết mới' : 'Sửa bài viết'}>
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading={!isEdit ? 'Thêm mới bài viết mới' : 'Sửa bài viết'}
          links={[
            { name: 'Bảng điều khiển', href: PATH_DASHBOARD.root },
            { name: 'Tin tức', href: PATH_DASHBOARD.blog.root },
            { name: !isEdit ? 'Thêm mới bài viết mới' : 'Sửa bài viết' },
          ]}
        />
        {
          isReady ? <BlogNewPostForm isEdit={isEdit} currentBlog={currentBlog} /> :  (
            <Box sx={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <CircularProgress />
            </Box>
          )
        }
      </Container>
    </Page>
  );
}
