import {useState, useEffect, useRef} from 'react';
// @mui
import { useTheme } from '@mui/material/styles';
import {
  Box,
  Card,
  Table,
  TableRow,
  Checkbox,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button,
} from '@mui/material';
// utils
import {useSnackbar} from "notistack";
import { fDate } from '../../utils/formatTime';
import { fCurrency } from '../../utils/formatNumber';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import Label from '../../components/Label';
import Image from '../../components/Image';
import Scrollbar from '../../components/Scrollbar';
import SearchNotFound from '../../components/SearchNotFound';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// sections
import {
  ProductMoreMenu,
  ProductListHead,
  ProductListToolbar,
} from '../../sections/@dashboard/e-commerce/product-list';
import axios from "../../utils/axios";

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Tên', alignRight: false },
  { id: 'createdAt', label: 'Ngày tạo', alignRight: false },
  { id: 'inventoryType', label: 'Trạng thái', alignRight: false },
  { id: 'price', label: 'Giá', alignRight: false },
  { id: 'priceSale', label: 'Giá khuyến mãi', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

export default function EcommerceProductList() {
  const { themeStretch } = useSettings();
  const theme = useTheme();
  const { enqueueSnackbar } = useSnackbar();

  const [productList, setProductList] = useState([]);
  const [page, setPage] = useState(0);
  const [selected, setSelected] = useState([]);
  const [filterName, setFilterName] = useState('');
  const [totalPage, setTotalPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [isOpenConfirmDeleteDialog, setIsOpenConfirmDeleteDialog] = useState(false);
  const selectingItemToDelete = useRef();

  useEffect(() => {
    const getProductList = async () => {
      try {
        const response = await axios.get( `/products?limit=${rowsPerPage}&page=${page + 1}`);
        setProductList(response.data.data);
        setTotalPage(response.data.totalPages);
      } catch (e) {
        console.log(e);
      }
    };
    getProductList();
  }, [page, rowsPerPage]);

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (filterName) => {
    setFilterName(filterName);
  };

  const handleOpenConfirmDeleteDialog = () => {
    setIsOpenConfirmDeleteDialog(true);
  };

  const handleCloseConfirmDeleteDialog = () => {
    setIsOpenConfirmDeleteDialog(false);
  };

  const handleAgreeDeleteDialog = async () => {
    const productId = selectingItemToDelete.current;
    try {
      await axios.delete(`/products/${productId}`);
      const deleteProduct = productList.filter((product) => product._id !== productId);
      setSelected([]);
      setProductList(deleteProduct);
      enqueueSnackbar(`Xóa thành công`);
    } catch (e) {
      enqueueSnackbar(`Đã xảy ra sự cố: ${e?.message}`, {variant: 'error'});
      console.log(e);
    }
    handleCloseConfirmDeleteDialog();
  };

  const handleDeleteProduct = (productId) => {
    selectingItemToDelete.current = productId;
    handleOpenConfirmDeleteDialog();
  };

  const handleDeleteProducts = (selected) => {
    const deleteProducts = productList.filter((product) => !selected.includes(product.name));
    setSelected([]);
    setProductList(deleteProducts);
  };

  return (
    <Page title="Danh sách sản phẩm">
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading="Danh sách sản phẩm"
          links={[
            { name: 'Bảng điều khiển', href: PATH_DASHBOARD.root },
            {
              name: 'Sản phẩm',
              href: PATH_DASHBOARD.product.root,
            },
            { name: 'Danh sách sản phẩm' },
          ]}
        />

        <Card>
          <ProductListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
            onDeleteProducts={() => handleDeleteProducts(selected)}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <ProductListHead
                  headLabel={TABLE_HEAD}
                />

                <TableBody>
                  {productList?.map((row) => {
                    const { _id, name, images, price, priceSale, createdAt, isActive } = row;

                    const isItemSelected = selected.indexOf(name) !== -1;

                    return (
                      <TableRow
                        hover
                        key={_id}
                        tabIndex={-1}
                        role="checkbox"
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell sx={{ display: 'flex', alignItems: 'center' }}>
                          <Image
                            disabledEffect
                            alt={name}
                            src={images[0]}
                            sx={{ borderRadius: 1.5, width: 64, height: 64, mr: 2 }}
                          />
                          <Typography variant="subtitle2" noWrap>
                            {name.length > 50 ?
                              `${name.substring(0, 50)}...` : name
                            }
                          </Typography>
                        </TableCell>
                        <TableCell style={{ minWidth: 160 }}>{fDate(createdAt)}</TableCell>
                        <TableCell style={{ minWidth: 160 }}>
                          <Label
                            variant={theme.palette.mode === 'light' ? 'ghost' : 'filled'}
                            color={
                              isActive ? 'success' : 'warning'
                            }
                          >
                            {isActive ? 'Hiển thị' : 'Ẩn'}
                          </Label>
                        </TableCell>
                        <TableCell>{fCurrency(price)}đ</TableCell>
                        <TableCell>{fCurrency(priceSale)}đ</TableCell>
                        <TableCell align="right">
                          <ProductMoreMenu productId={_id} onDelete={() => handleDeleteProduct(_id)} />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>

                {productList.length === 0 && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6}>
                        <Box sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </Box>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            labelRowsPerPage="Số hàng trên mỗi trang"
            rowsPerPageOptions={[1, 5, 10, 25]}
            component="div"
            count={totalPage}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={(event, value) => setPage(value)}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      <Dialog
        open={isOpenConfirmDeleteDialog}
        onClose={handleCloseConfirmDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bạn có chắc chắn muốn xóa?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Lưu ý: Sau khi xóa sẽ không thể khôi phục lại.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmDeleteDialog}>Từ chối</Button>
          <Button onClick={handleAgreeDeleteDialog} autoFocus>
            Đồng ý
          </Button>
        </DialogActions>
      </Dialog>
    </Page>
  );
}

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  if (query) {
    return array.filter((_product) => _product.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }

  return stabilizedThis.map((el) => el[0]);
}
