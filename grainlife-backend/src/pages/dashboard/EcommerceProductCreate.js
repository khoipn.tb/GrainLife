import {useEffect, useState} from 'react';
import { useParams, useLocation } from 'react-router-dom';
// @mui
import {Box, Container} from '@mui/material';
// redux
import CircularProgress from '@mui/material/CircularProgress';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import ProductNewForm from '../../sections/@dashboard/e-commerce/ProductNewForm';
import axios from "../../utils/axios";

// ----------------------------------------------------------------------

export default function EcommerceProductCreate() {
  const { themeStretch } = useSettings();
  const { pathname } = useLocation();
  const { id } = useParams();
  const isEdit = pathname.includes('edit');
  const [currentProduct, setCurrentProduct] = useState(null);
  const [categories, setCategories] = useState(null);
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    const getProduct = async () => {
      try {
        const response = await axios.get(`/products/${id}`);
        setCurrentProduct(response.data.data);
      } catch (e) {
        console.log(e);
      }
    };

    const getCategories = async () => {
      try {
        const response = await axios.get(`/categories`);
        setCategories(response.data.data);
      } catch (e) {
        console.log(e);
      }
    };

    const prepare = async () => {
      try {
        if (isEdit) {
          await getProduct();
          await getCategories();
          setIsReady(true);
        } else {
          await getCategories();
          setIsReady(true);
          setCurrentProduct(null);
        }
      } catch (e) {
        console.log(e)
      }
    };

    prepare()
  }, [id, isEdit]);

  return (
    <Page title={!isEdit ? 'Thêm mới sản phẩm' : 'Sửa sản phẩm'}>
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading={!isEdit ? 'Thêm mới sản phẩm' : 'Sửa sản phẩm'}
          links={[
            { name: 'Trang quản trị', href: PATH_DASHBOARD.root },
            {
              name: 'Sản phẩm',
              href: PATH_DASHBOARD.product.root,
            },
            { name: !isEdit ? 'Thêm mới sản phẩm' : currentProduct?.name },
          ]}
        />
        {
          isReady ? <ProductNewForm isEdit={isEdit} currentProduct={currentProduct} categories={categories} /> :  (
            <Box sx={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <CircularProgress />
            </Box>
          )
        }

      </Container>
    </Page>
  );
}
