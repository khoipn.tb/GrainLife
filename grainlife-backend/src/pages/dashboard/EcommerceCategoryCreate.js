import {useEffect, useState} from 'react';
import {useParams, useLocation} from 'react-router-dom';
// @mui
import {Container} from '@mui/material';
// routes
import {PATH_DASHBOARD} from '../../routes/paths';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import CategoryNewForm from '../../sections/@dashboard/e-commerce/CategoryNewForm';
import axios from "../../utils/axios";

// ----------------------------------------------------------------------

export default function EcommerceCategoryCreate() {
	const {themeStretch} = useSettings();
	const {pathname} = useLocation();
	const {id} = useParams();
	const isEdit = pathname.includes('edit');
	const [isReady, setIsReady] = useState(false);
	const [currentCategory, setCurrentCategory] = useState(null);


	useEffect(() => {
		const getCategory = async () => {
			try {
				const response = await axios.get(`/categories/${id}`);
				setCurrentCategory(response.data.data);
			} catch (e) {
				console.log(e);
			}
		};

		const prepare = async () => {
			if (isEdit) {
				try {
					await getCategory();
					setIsReady(true);
					// eslint-disable-next-line no-empty
				} catch (e) {}
			} else {
				setIsReady(true);
				setCurrentCategory(null);
			}
		};
		prepare();
	}, [id, isEdit]);

	return (
		<Page title={!isEdit ? 'Thêm mới danh mục' : 'Sửa danh mục'}>
			<Container maxWidth={themeStretch ? false : 'lg'}>
				<HeaderBreadcrumbs
					heading={!isEdit ? 'Thêm mới danh mục' : 'Sửa danh mục'}
					links={[
						{name: 'Trang quản trị', href: PATH_DASHBOARD.root},
						{
							name: 'Danh mục',
							href: PATH_DASHBOARD.product.root,
						},
						{name: !isEdit ? 'Thêm mới danh mục' : currentCategory?.name},
					]}
				/>
				{
					isReady ? <CategoryNewForm isEdit={isEdit} currentCategory={currentCategory}/> : null
				}
			</Container>
		</Page>
	);
}
