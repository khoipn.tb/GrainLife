import { createSlice } from '@reduxjs/toolkit';
// utils
import axios from '../../utils/axios';
//
import { dispatch } from '../store';
import {setSession} from "../../utils/jwt";

// ----------------------------------------------------------------------

const initialState = {
  accessToken: '',
  userInfo: null,
  isLogging: false,
  error: null,
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    START_LOGIN(state) {
      state.isLogging = true;
      state.error = null;
    },
    LOGIN(state, action) {
      state.accessToken = action.payload.accessToken;
      state.userInfo = action.payload.userInfo;
      state.isLogging = false;
      state.error = null;
    },
    LOGOUT(state) {
      state.accessToken = '';
      state.userInfo = null;
      state.isLogging = false;
      state.error = null;
    },
    HAS_ERROR(state, action) {
      state.error = action.payload;
      state.isLogging = false;
    }
  },
});

// Reducer
export default slice.reducer;

// Actions
export const {
  LOGIN,
  LOGOUT,
  HAS_ERROR
} = slice.actions;

// ----------------------------------------------------------------------

export function login(email, password) {
  return async () => {
    try {
      dispatch(slice.actions.START_LOGIN());
      const response = await axios.post('/auth/login', {email, password});
      dispatch(slice.actions.LOGIN({accessToken: response.data.accessToken, userInfo: response.data.userInfo,}));
      localStorage.setItem('accessToken', response.data.accessToken);
    } catch (error) {
      dispatch(slice.actions.HAS_ERROR(error.message));
    }
  };
}

export function logout() {
  return () => {
    dispatch(slice.actions.LOGOUT());
    localStorage.removeItem('accessToken');
  };
}

// ----------------------------------------------------------------------
