import { createSlice } from '@reduxjs/toolkit';
// utils
import axios from '../../utils/axios';
//
import { dispatch } from '../store';

// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  error: null,
  categories: [],
  category: null,
};

const slice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    START_LOADING(state) {
      state.isLoading = true;
    },

    HAS_ERROR(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    GET_CATEGORIES_SUCCESS(state, action) {
      state.isLoading = false;
      state.categories = action.payload;
    },

    GET_CATEGORY_SUCCESS(state, action) {
      state.isLoading = false;
      state.category = action.payload;
    },
  },
});

// Reducer
export default slice.reducer;

// Actions
// eslint-disable-next-line no-empty-pattern
export const {} = slice.actions;

// ----------------------------------------------------------------------

export function getCategories() {
  return async () => {
    dispatch(slice.actions.START_LOADING());
    try {
      const response = await axios.get('/categories');
      dispatch(slice.actions.GET_CATEGORIES_SUCCESS(response.data.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

// ----------------------------------------------------------------------

export function getCategory(id) {
  return async () => {
    dispatch(slice.actions.START_LOADING());
    try {
      const response = await axios.get(`/categories/${id}`,);
      dispatch(slice.actions.GET_CATEGORY_SUCCESS(response.data.product));
    } catch (error) {
      console.error(error);
      dispatch(slice.actions.HAS_ERROR(error));
    }
  };
}
