import * as Yup from 'yup';
import { useCallback, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
// form
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
// @mui
import { LoadingButton } from '@mui/lab';
import { styled } from '@mui/material/styles';
import { Grid, Card, Stack, Button, Typography } from '@mui/material';
// routes
import PropTypes from "prop-types";
import { PATH_DASHBOARD } from '../../../routes/paths';
// components
import { RHFSwitch, RHFEditor, FormProvider, RHFTextField, RHFUploadSingleFile } from '../../../components/hook-form';
//
import BlogNewPostPreview from './BlogNewPostPreview';
import axios from "../../../utils/axios";

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1),
}));

BlogNewPostForm.propTypes = {
  isEdit: PropTypes.bool,
  currentBlog: PropTypes.object,
};

// ----------------------------------------------------------------------

export default function BlogNewPostForm({isEdit, currentBlog}) {
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const handleOpenPreview = () => {
    setOpen(true);
  };

  const handleClosePreview = () => {
    setOpen(false);
  };

  const NewBlogSchema = Yup.object().shape({
    title: Yup.string().required('Tiêu đề là bắt buộc'),
    content: Yup.string().required('Nội dung là bắt buộc'),
    cover: Yup.mixed().required('Ảnh bìa là bắt buộc'),
  });

  const defaultValues = {
    title: currentBlog?.title || '',
    content: currentBlog?.content || '',
    cover: currentBlog?.cover || '',
    isActive: typeof currentBlog?.isActive === "boolean" ? currentBlog?.isActive : true,
    enableComment: typeof currentBlog?.enableComment === "boolean" ? currentBlog?.enableComment : true,
    metaTitle: currentBlog?.metaTitle || '',
    metaDescription: currentBlog?.metaDescription || '',
    metaKeywords: currentBlog?.metaKeywords || '',
  };

  const methods = useForm({
    resolver: yupResolver(NewBlogSchema),
    defaultValues,
  });

  const {
    reset,
    watch,
    getValues,
    setValue,
    handleSubmit,
    formState: { isSubmitting, isValid },
  } = methods;

  const values = watch();

  const onSubmit = async () => {
    try {
      if (!isEdit) {
        const formData = new FormData();
        formData.append("title", getValues("title"));
        formData.append("content", getValues("content"));
        formData.append("metaTitle", getValues("metaTitle") || '');
        formData.append("metaDescription", getValues("metaDescription"));
        formData.append("metaKeywords", getValues("metaKeywords"));
        formData.append("enableComment", getValues("enableComment"));
        formData.append("isActive", getValues("isActive"));
        formData.append("cover", getValues("cover"));

        await axios.post('/blogs', formData);
        reset();
        enqueueSnackbar('Tạo thành công!');
        navigate(PATH_DASHBOARD.blog.list);
      } else {
        const formData = new FormData();
        formData.append("title", getValues("title"));
        formData.append("content", getValues("content"));
        formData.append("metaTitle", getValues("metaTitle"));
        formData.append("metaDescription", getValues("metaDescription"));
        formData.append("metaKeywords", getValues("metaKeywords"));
        formData.append("enableComment", getValues("enableComment"));
        formData.append("isActive", getValues("isActive"));
        formData.append("cover", getValues("cover"));

        await axios.patch(`/blogs/${currentBlog._id}`, formData);
        reset();
        enqueueSnackbar('Cập nhật thành công!');
        navigate(PATH_DASHBOARD.blog.list);
      }
      handleClosePreview();
    } catch (error) {
      console.error(error);
    }
  };

  const handleDrop = useCallback(
    (acceptedFiles) => {
      const file = acceptedFiles[0];

      if (file) {
        setValue(
          'cover',
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        );
      }
    },
    [setValue]
  );


  return (
    <>
      <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <RHFTextField name="title" label="Tiêu đề" />
                <div>
                  <LabelStyle>Nội dung</LabelStyle>
                  <RHFEditor name="content" />
                </div>

                <div>
                  <LabelStyle>Ảnh bìa</LabelStyle>
                  <RHFUploadSingleFile name="cover" accept="image/*" maxSize={3145728} onDrop={handleDrop} />
                </div>
              </Stack>
            </Card>
          </Grid>

          <Grid item xs={12} md={4}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <div>
                  <RHFSwitch
                    name="isActive"
                    label="Hiển thị"
                    labelPlacement="start"
                    sx={{ mb: 1, mx: 0, width: 1, justifyContent: 'space-between' }}
                  />

                  <RHFSwitch
                    name="enableComment"
                    label="Cho phép bình luận"
                    labelPlacement="start"
                    sx={{ mx: 0, width: 1, justifyContent: 'space-between' }}
                  />
                </div>

                <RHFTextField name="metaTitle" label="Tiêu đề meta" />

                <RHFTextField name="metaDescription" label="Mô tả meta" fullWidth multiline rows={3} />

                <RHFTextField name="metaKeywords" label="Từ khóa meta" fullWidth multiline rows={3} />

              </Stack>
            </Card>

            <Stack direction="row" spacing={1.5} sx={{ mt: 3 }}>
              <Button fullWidth color="inherit" variant="outlined" size="large" onClick={handleOpenPreview}>
                Xem trước
              </Button>
              <LoadingButton fullWidth type="submit" variant="contained" size="large" loading={isSubmitting}>
                Đăng bài
              </LoadingButton>
            </Stack>
          </Grid>
        </Grid>
      </FormProvider>

      <BlogNewPostPreview
        values={values}
        isOpen={open}
        isValid={isValid}
        isSubmitting={isSubmitting}
        onClose={handleClosePreview}
        onSubmit={handleSubmit(onSubmit)}
      />
    </>
  );
}
