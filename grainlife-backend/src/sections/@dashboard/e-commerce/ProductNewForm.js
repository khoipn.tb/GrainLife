import PropTypes from 'prop-types';
import * as Yup from 'yup';
import {useSnackbar} from 'notistack';
import {useNavigate} from 'react-router-dom';
import {useCallback, useEffect, useMemo} from 'react';
// form
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
// @mui
import {styled} from '@mui/material/styles';
import {LoadingButton} from '@mui/lab';
import {Card, Grid, Stack, Typography, InputAdornment} from '@mui/material';
// routes
import {PATH_DASHBOARD} from '../../../routes/paths';
// components
import {
	FormProvider,
	RHFSwitch,
	RHFSelect,
	RHFEditor,
	RHFTextField,
	RHFUploadMultiFile,
} from '../../../components/hook-form';
import axios from "../../../utils/axios";

// ----------------------------------------------------------------------

const LabelStyle = styled(Typography)(({theme}) => ({
	...theme.typography.subtitle2,
	color: theme.palette.text.secondary,
	marginBottom: theme.spacing(1),
}));

// ----------------------------------------------------------------------

ProductNewForm.propTypes = {
	isEdit: PropTypes.bool,
	currentProduct: PropTypes.object,
	categories: PropTypes.array,
};

export default function ProductNewForm({isEdit, currentProduct, categories}) {
	const navigate = useNavigate();
	const {enqueueSnackbar} = useSnackbar();

	const NewProductSchema = Yup.object().shape({
		name: Yup.string().required('Tên là bắt buộc'),
		description: Yup.string().required('Mô tả là bắt buộc'),
		images: Yup.array().min(1, 'Ảnh tả là bắt buộc'),
		price: Yup.number().moreThan(0, 'Giá phải lớn hơn 0'),
		priceSale: Yup.number(),
		inStock: Yup.bool(),
		isActive: Yup.bool(),
		category: Yup.string(),
	});

	const defaultValues = useMemo(
		() => ({
			name: currentProduct?.name || '',
			description: currentProduct?.description || '',
			images: currentProduct?.images || [],
			price: currentProduct?.price || 0,
			priceSale: currentProduct?.priceSale || 0,
			inStock: typeof currentProduct?.inStock === "boolean" ? currentProduct?.inStock : true,
			category: currentProduct?.category || categories[0]?._id,
			isActive: typeof currentProduct?.isActive === "boolean" ? currentProduct?.isActive : true,
		}),
		[categories, currentProduct]
	);

	const methods = useForm({
		resolver: yupResolver(NewProductSchema),
		defaultValues,
	});

	const {
		reset,
		watch,
		setValue,
		getValues,
		handleSubmit,
		formState: {isSubmitting},
	} = methods;

	const values = watch();

	useEffect(() => {
		if (isEdit && currentProduct) {
			reset(defaultValues);
		}
		if (!isEdit) {
			reset(defaultValues);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isEdit, currentProduct]);

	const onSubmit = async () => {
		try {
			if (!isEdit) {
				const formData = new FormData();
				formData.append("name", getValues("name"));
				formData.append("description", getValues("description"));
				formData.append("price", getValues("price"));
				formData.append("priceSale", getValues("priceSale"));
				formData.append("inStock", getValues("inStock"));
				formData.append("category", getValues("category"));
				formData.append("isActive", getValues("isActive"));
				getValues("images").forEach((file) => {
					formData.append("images", file);
				});

				await axios.post('/products', formData);
				reset();
				enqueueSnackbar('Tạo thành công!');
				navigate(PATH_DASHBOARD.product.list);
			} else {
				const formData = new FormData();
				formData.append("name", getValues("name"));
				formData.append("description", getValues("description"));
				formData.append("price", getValues("price"));
				formData.append("priceSale", getValues("priceSale"));
				formData.append("inStock", getValues("inStock"));
				formData.append("category", getValues("category"));
				formData.append("isActive", getValues("isActive"));
				getValues("images").forEach((file) => {
					if (file instanceof File) {
						formData.append("images", file);
					}
				});

				await axios.patch(`/products/${currentProduct._id}`, formData);
				reset();
				enqueueSnackbar('Cập nhật thành công!');
				navigate(PATH_DASHBOARD.product.list);
			}
		} catch (error) {
			enqueueSnackbar(JSON.stringify(error), {variant: 'error'})
			console.error(error);
		}
	};

	const handleDrop = useCallback(
		(acceptedFiles) => {
			setValue(
				'images',
				acceptedFiles.map((file) =>
					Object.assign(file, {
						preview: URL.createObjectURL(file),
					})
				)
			);
		},
		[setValue]
	);

	const handleRemoveAll = () => {
		setValue('images', []);
	};

	const handleRemove = (file) => {
		const filteredItems = values.images?.filter((_file) => _file !== file);
		setValue('images', filteredItems);
	};

	return (
		<FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
			<Grid container spacing={3}>
				<Grid item xs={12} md={8}>
					<Card sx={{p: 3}}>
						<Stack spacing={3}>
							<RHFTextField name="name" label="Tên sản phẩm"/>

							<div>
								<LabelStyle>Mô tả</LabelStyle>
								<RHFEditor name="description"/>
							</div>

							<div>
								<LabelStyle>Ảnh</LabelStyle>
								<RHFUploadMultiFile
									name="images"
									showPreview
									accept="image/*"
									maxSize={3145728}
									onDrop={handleDrop}
									onRemove={handleRemove}
									onRemoveAll={handleRemoveAll}
								/>
							</div>
						</Stack>
					</Card>
				</Grid>

				<Grid item xs={12} md={4}>
					<Stack spacing={3}>
						<Card sx={{p: 3}}>
							<Stack spacing={3} mb={2}>
								<div>
									<LabelStyle>Tình trạng</LabelStyle>
									<RHFSwitch name="inStock" label="Còn hàng"/>
								</div>
								<RHFSelect name="category" label="Danh mục">
									{categories.map((category) => (
										<option key={category._id} value={category._id}>
											{category.name}
										</option>
									))}
								</RHFSelect>
								<RHFTextField
									name="price"
									label="Giá"
									placeholder="0.00"
									value={getValues('price') === 0 ? '' : getValues('price')}
									InputLabelProps={{shrink: true}}
									InputProps={{
										endAdornment: <InputAdornment position="start">đ</InputAdornment>,
										type: 'number',
									}}
								/>

								<RHFTextField
									name="priceSale"
									label="Giá khuyến mãi"
									placeholder="0.00"
									value={getValues('priceSale') === 0 ? '' : getValues('priceSale')}
									InputLabelProps={{shrink: true}}
									InputProps={{
										endAdornment: <InputAdornment position="start">đ</InputAdornment>,
										type: 'number',
									}}
								/>
							</Stack>

							<RHFSwitch name="isActive" label="Hiển thị"/>
						</Card>

						<LoadingButton type="submit" variant="contained" size="large" loading={isSubmitting}>
							{!isEdit ? 'Tạo sản phẩm' : 'Lưu thay đổi'}
						</LoadingButton>
					</Stack>
				</Grid>
			</Grid>
		</FormProvider>
	);
}
