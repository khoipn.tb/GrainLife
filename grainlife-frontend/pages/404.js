import Layout from "../components/layout/Layout";
import Link from "next/link"
function Custom404() {
    return (
        <>
            <Layout
              noBreadcrumb="d-none"
            >
                <main className="main page-404">
                    <div className="page-content pt-150 pb-150">
                        <div className="container">
                            <div className="row">
                                <div className="col-xl-8 col-lg-10 col-md-12 m-auto text-center">
                                    <p className="mb-20">
                                        <img
                                            src="assets/imgs/page/page-404.png"
                                            alt=""
                                            className="hover-up"
                                        />
                                    </p>
                                    <h1 className="display-2 mb-30">
                                        Không tìm thấy trang
                                    </h1>
                                    <p className="font-lg text-grey-700 mb-30">
                                        Liên kết bạn nhấp vào có thể bị hỏng hoặc trang có thể đã bị xóa.
                                        <br />
                                        truy cập {" "}
                                        <Link href="/"><a>
                                            {" "}
                                            <span> Trang chủ </span>
                                        </a></Link>
                                        hoặc{" "}
                                        <Link href="/page-contact"><a>
                                            <span> Liên hệ </span>
                                        </a></Link>
                                        với chúng tôi về vấn đề
                                    </p>
                                    <Link href="/"><a
                                        className="btn btn-default submit-auto-width font-xs hover-up mt-30"

                                    >
                                        <i className="fi-rs-home mr-5"></i> Quay lại trang chủ
                                        </a></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </Layout>
        </>
    );
}

export default Custom404;
