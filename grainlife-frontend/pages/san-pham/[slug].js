import React from "react";
import ProductDetails from "../../components/ecommerce/ProductDetails";
import Layout from '../../components/layout/Layout';
import axios from "../../util/axios";

const ProductId = ({ product }) => {
    return (
        <>
        <Layout parent="Trang chủ" sub="Sản phẩm" subChild={product.name}>
            <div className="container">
                <ProductDetails product={product} />
            </div>
        </Layout>
        </>
    );
};



export async function getStaticProps ({params}) {
    try {
        const response = await axios.get(`/products/${params.slug}`);
        if (!response.data.data) {
            return {
                notFound: true,
            }
        }

        return {
            props: {
                product: response.data.data,
            },
            revalidate: 30,
        }
    } catch (e) {
        console.log(e);
    }
}

export async function getStaticPaths() {
    const response = await axios.get(`/products`);
    const paths = response.data.data.map((product) => ({
        params: { slug: product.slug }
    }))

    return { paths, fallback: 'blocking' }
}

export default ProductId;
