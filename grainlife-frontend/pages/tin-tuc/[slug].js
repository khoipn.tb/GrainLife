import BlogSingle from '../../components/elements/BlogSingle';
import Layout from "../../components/layout/Layout";
import axios from "../../util/axios";

function BlogDetail({blog}) {
	return (
		<>
			<Layout noBreadcrumb="d-none">
				<section className="mt-50 mb-50">
					<div className="container custom">
						<div className="row">
							<div className="col-lg-10 m-auto">
								<BlogSingle blog={blog}/>
							</div>
						</div>
					</div>
				</section>
			</Layout>
		</>
	);
}

export async function getStaticProps ({params}) {
	try {
		const response = await axios.get(`/blogs/${params.slug}`);
		if (!response.data.data) {
			return {
				notFound: true,
			}
		}
		return {
			props: {
				blog: response.data.data,
			},
			revalidate: 30,
		}
	} catch (e) {
		console.log(e);
	}
}

export async function getStaticPaths() {
	const response = await axios.get(`/blogs?&limit=9999`);
	const paths = response.data.data.map((blog) => ({
		params: { slug: blog.slug }
	}))

	return { paths, fallback: 'blocking' }
}


export default BlogDetail;
