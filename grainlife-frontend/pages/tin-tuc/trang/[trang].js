import BlogGrid from "../../../components/elements/BlogGrid";
import Layout from "../../../components/layout/Layout";
import React from "react";
import axios from "../../../util/axios";
import Pagination from "../../../components/ecommerce/Pagination";
import { useRouter } from "next/router";

function Blogs({blogs, pagination: {totalPage, currentPage}}) {
	const router = useRouter();

	let showPagination = 4;
	let pagination = new Array(totalPage)
		.fill()
		.map((_, idx) => idx + 1);

	let start = Math.floor((currentPage - 1) / showPagination) * showPagination;
	let end = start + showPagination;
	const getPaginationGroup = pagination.slice(start, end);

	const next = () => {
		router.push(`/tin-tuc/trang/${currentPage + 1}`);
	};

	const prev = () => {
		router.push(`/tin-tuc/trang/${currentPage - 1}`);
	};

	const handleActive = (page) => {
		router.push(`/tin-tuc/trang/${page}`);
	};
	return (
		<>
			<Layout noBreadcrumb="d-none">
				<section className="mt-50 mb-50">
					<div className="container custom">
						<div className="row">
							<div className="col-lg-12">
								<div className="shop-product-fillter mb-50 pr-30">
									<h2>
										Tin tức
									</h2>
								</div>
								<div className="loop-grid pr-30">
									<div className="row">
										<BlogGrid blogs={blogs} />
									</div>
								</div>
								<div className="pagination-area mt-15 mb-sm-5 mb-lg-0">
									<nav aria-label="Page navigation example">
										<Pagination
											getPaginationGroup={
												getPaginationGroup
											}
											currentPage={currentPage}
											pages={totalPage}
											next={next}
											prev={prev}
											handleActive={handleActive}
										/>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</section>
			</Layout>
		</>
	);
}

export async function getStaticPaths() {
	const response = await axios.get(`/blogs`);
	const paths = new Array(response.data.totalPages).fill().map((_, idx) => ({params: { trang: (idx + 1).toString() }}))
	return {
		paths,
		fallback: "blocking",
	};
}

export async function getStaticProps (context) {
	const response = await axios.get(`/blogs?page=${context.params.trang || 1}&limit=1`);
	return {
		props: {
			blogs: response.data.data,
			pagination: {
				totalPage: response.data.totalPages,
				currentPage: response.data.currentPage || context.params.trang || 1,
			},
		},
		revalidate: 30,
	}
}

export default Blogs;
