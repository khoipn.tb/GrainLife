import Breadcrumb2 from "../../../../components/layout/Breadcrumb2";
import QuickView from "../../../../components/ecommerce/QuickView";
import SingleProduct from "../../../../components/ecommerce/SingleProduct";
import Layout from "../../../../components/layout/Layout";
import axios from "../../../../util/axios";
import Link from "next/link";
import Pagination from "../../../../components/ecommerce/Pagination";
import React from "react";
import {useRouter} from "next/router";

const CategorySlug = ({ category, slug, categories, pagination: {totalPage, currentPage} }) => {
    const router = useRouter();

    let showPagination = 6;
    let pagination = new Array(totalPage)
      .fill()
      .map((_, idx) => idx + 1);

    let start = Math.floor((currentPage - 1) / showPagination) * showPagination;
    let end = start + showPagination;
    const getPaginationGroup = pagination.slice(start, end);

    const next = () => {
        router.push(`/danh-muc/${slug}/trang/${currentPage + 1}`);
    };

    const prev = () => {
        router.push(`/danh-muc/${slug}/trang/${currentPage - 1}`);
    };

    const handleActive = (page) => {
        router.push(`/danh-muc/${slug}/trang/${page}`);
    };

    return (
        <>
            <Layout noBreadcrumb="d-none">
            <Breadcrumb2 title={category.name} parent="Trang chủ" sub="Cửa hàng" subChild={category.name}/>
                <section className="mt-50 mb-50">
                    <div className="container mb-30">
                        <div className="row flex-row-reverse">
                            <div className="col-lg-4-5">
                                <div className="shop-product-fillter">
                                    <div className="totall-product">
                                        <p>
                                            Chúng tôi đã tìm thấy
                                            <strong className="text-brand">
                                                {category?.products?.length || 0}
                                            </strong>
                                            mặt hàng cho bạn!
                                        </p>
                                    </div>
                                </div>
                                <div className="row product-grid">
                                    {category.products.length === 0 && (
                                        <h3 className="mb-50">Không tìm thấy sản phẩm nào</h3>
                                    )}

                                    {category?.products?.map((item, idx) => (
                                        <div
                                            className="col-lg-1-5 col-md-4 col-12 col-sm-6"
                                            key={`${item._id}-${idx}`}
                                        >
                                            <SingleProduct product={item} />
                                        </div>
                                    ))}
                                </div>
                                <div className="pagination-area mt-15 mb-sm-5 mb-lg-0">
                                    <nav aria-label="Page navigation example">
                                        <Pagination
                                          getPaginationGroup={
                                              getPaginationGroup
                                          }
                                          currentPage={currentPage}
                                          pages={totalPage}
                                          next={next}
                                          prev={prev}
                                          handleActive={handleActive}
                                        />
                                    </nav>
                                </div>
                            </div>
                            <div className="col-lg-1-5 primary-sidebar sticky-sidebar">
                                <div className="sidebar-widget widget-category-2 mb-30">
                                    <h5 className="section-title style-1 mb-30">
                                        Danh mục
                                    </h5>
                                    <ul>
                                        <Link href="/danh-muc/[slug]/trang/[trang]" as="/danh-muc/tat-ca-san-pham/trang/1">
                                            <li>
                                               <a>Tất cả sản phẩm</a>
                                            </li>
                                        </Link>
                                        {
                                            categories?.map((cate, idx) => {
                                                return (
                                                  <Link key={`${cate._id}-${idx}`} href={`/danh-muc/${cate.slug}/trang/1`}>
                                                      <li>
                                                            <a>{cate.name}</a>
                                                      </li>
                                                  </Link>
                                                );
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <QuickView />
            </Layout>
        </>
    );
};

const allProductSlug = 'tat-ca-san-pham';
const limitItem = 1;

export async function getStaticPaths() {
    const categoriesResponse = await axios.get(`/categories`);
    let paths = [];
    for (const category of categoriesResponse.data.data) {
        const response = await axios.get(`/categories/${category._id}/products?limit=${limitItem}`);
         new Array(response.data.totalPages === 0 ? 1 : response.data.totalPages)
          .fill()
          .forEach((_, idx) => {
              paths.push({params: { slug: category.slug, trang: (idx + 1).toString() }});
          });
    }

    const response = await axios.get(`/products?limit=${limitItem}`);
    new Array(response.data.totalPages === 0 ? 1 : response.data.totalPages)
      .fill()
      .forEach((_, idx) => {
          paths.push({params: { slug: allProductSlug, trang: (idx + 1).toString() }});
      });

    return { paths, fallback: 'blocking' }
}


export async function getStaticProps (context) {
    const categoriesResponse = await axios.get("/categories");
    const {slug = allProductSlug, trang} = context.params;
    if (slug === allProductSlug) {
        const response = await axios.get(`/products?limit=${limitItem}&page=${trang || 1}`);
        return {
            props: {
                category: {
                    name: 'Tất cả sản phẩm',
                    products: response.data.data,
                },
                pagination: {
                    totalPage: response.data.totalPages,
                    currentPage: response.data.currentPage || context.params.trang || 1,
                },
                slug,
                categories: categoriesResponse.data.data
            },
            revalidate: 30,
        }
    } else {
        const response = await axios.get(`/categories/${slug}/products?limit=${limitItem}&page=${trang || 1}`);
        return {
            props: {
                category: {
                    ...response.data.category,
                    products: response.data.data,
                },
                pagination: {
                    totalPage: response.data.totalPages,
                    currentPage: response.data.currentPage || context.params.trang || 1,
                },
                slug,
                categories: categoriesResponse.data.data
            },
            revalidate: 30,
        }
    }
}

export default CategorySlug;
