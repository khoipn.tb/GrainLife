import { connect } from "react-redux";
import Layout from "../components/layout/Layout";

import Link from "next/link";
import {
    clearCart,
    closeCart,
    decreaseQuantity,
    deleteFromCart,
    increaseQuantity,
    openCart
} from "../redux/action/cart";
import {fCurrency} from "../util/formatNumber";

const Cart = ({
    openCart,
    cartItems,
    activeCart,
    closeCart,
    increaseQuantity,
    decreaseQuantity,
    deleteFromCart,
    clearCart,
}) => {
    const price = () => {
        let price = 0;
        cartItems.forEach((item) => (price += item.price * item.quantity));

        return price;
    };

    return (
        <>
            <Layout parent="Trang chủ" sub="Cửa hàng" subChild="Giỏ hàng">
                <section className="mt-50 mb-50">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 mb-40">
                                <h1 className="heading-2 mb-10">Giỏ hàng của bạn</h1>
                                <div className="d-flex justify-content-between">
                                    {cartItems.length <= 0 ? (
                                      <h6 className="text-body">
                                          Không có sản phẩm nào trong giỏ hàng của bạn
                                      </h6>
                                    ) : (
                                      <h6 className="text-body">
                                          Có{" "}
                                          <span className="text-brand">{cartItems.length} </span>{" "}
                                          sản phẩm trong giỏ hàng của bạn
                                      </h6>
                                    )}
                                    {/*<h6 className="text-body">*/}
                                    {/*    <a href="#" className="text-muted">*/}
                                    {/*        <i className="fi-rs-trash mr-5"></i>*/}
                                    {/*        Xóa giỏ hàng*/}
                                    {/*    </a>*/}
                                    {/*</h6>*/}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="table-responsive shopping-summery">
                                    <table
                                        className={
                                            cartItems.length > 0
                                                ? "table table-wishlist"
                                                : "d-none"
                                        }
                                    >
                                        <thead>
                                            <tr className="main-heading">
                                                <th className="custome-checkbox start pl-30" colSpan="2">
                                                    Tên sản phẩm
                                                </th>
                                                <th scope="col">Đơn giá</th>
                                                <th scope="col">Số lượng</th>
                                                <th scope="col">Tổng phụ</th>
                                                <th scope="col" className="end">
                                                    Xóa
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {cartItems.map((item, i) => (
                                                <tr key={i}>
                                                    <td className="image product-thumbnail">
                                                        <img
                                                            src={
                                                                item.images[0]
                                                            }
                                                        />
                                                    </td>

                                                    <td className="product-des product-name">
                                                        <h6 className="product-name">
                                                            <Link href="/danh-muc/[slug]">
                                                                <a>
                                                                    {item.name}
                                                                </a>
                                                            </Link>
                                                        </h6>
                                                    </td>
                                                    <td
                                                        className="price"
                                                        data-title="Đơn giá"
                                                    >
                                                        <h4 className="text-brand">
                                                            {fCurrency(item.price)}
                                                        </h4>
                                                    </td>
                                                    <td
                                                        className="text-center detail-info"
                                                        data-title="Số lượng"
                                                    ><div className="detail-extralink mr-15">
                                                        <div className="detail-qty border radius ">
                                                            <a
                                                                onClick={(e) =>
                                                                    decreaseQuantity(
                                                                        item._id
                                                                    )
                                                                }
                                                                className="qty-down"
                                                            >
                                                                <i className="fi-rs-angle-small-down"></i>
                                                            </a>
                                                            <span className="qty-val">
                                                                {item.quantity}
                                                            </span>
                                                            <a
                                                                onClick={(e) =>
                                                                    increaseQuantity(
                                                                        item._id
                                                                    )
                                                                }
                                                                className="qty-up"
                                                            >
                                                                <i className="fi-rs-angle-small-up"></i>
                                                            </a>
                                                        </div>
                                                        </div>
                                                    </td>
                                                    <td
                                                        className="text-right"
                                                        data-title="Tổng phụ"
                                                    >
                                                        <h4 className="text-body">
                                                            {fCurrency(item.quantity * item.price)}
                                                        </h4>
                                                    </td>
                                                    <td
                                                        className="action"
                                                        data-title="Xóa"
                                                    >
                                                        <a
                                                            onClick={(e) =>
                                                                deleteFromCart(
                                                                    item._id
                                                                )
                                                            }
                                                            className="text-muted"
                                                        >
                                                            <i className="fi-rs-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            ))}
                                            <tr>
                                                <td
                                                    colSpan="6"
                                                    className="text-end"
                                                >
                                                    {cartItems.length > 0 && (
                                                        <a
                                                            onClick={clearCart}
                                                            className="text-muted"
                                                        >
                                                            <i className="fi-rs-cross-small"></i>
                                                            Xóa giỏ hàng
                                                        </a>
                                                    )}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="cart-action text-end">
                                    {/*<a className="btn ">*/}
                                    {/*    <i className="fi-rs-shopping-bag mr-10"></i>*/}
                                    {/*    Tiếp tục mua sắm*/}
                                    {/*</a>*/}
                                </div>
                                <div className="divider center_icon mt-50 mb-50">
                                    <i className="fi-rs-fingerprint"></i>
                                </div>
                                <div className="row mb-50 justify-content-end">
                                    <div className="col-lg-6 col-md-12">
                                        <div className="border p-md-4 p-30 border-radius cart-totals">
                                            <div className="heading_s1 mb-3">
                                                <h4>Tổng đơn hàng: {fCurrency(price())}</h4>
                                            </div>
                                            <button className="btn justify-content-end">
                                                <i className="fi-rs-box-alt mr-10"></i>
                                                Đặt hàng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

const mapStateToProps = (state) => ({
    cartItems: state.cart,
    activeCart: state.counter,
});

const mapDispatchToProps = {
    closeCart,
    increaseQuantity,
    decreaseQuantity,
    deleteFromCart,
    openCart,
    clearCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
