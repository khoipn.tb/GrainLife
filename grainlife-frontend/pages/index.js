import CategoryTab from "../components/ecommerce/categoryTab";
import QuickView from "./../components/ecommerce/QuickView";
import Layout from "./../components/layout/Layout";
import Intro1 from "./../components/sliders/Intro1";
import axios from "../util/axios";
import CommitmentToQuality from "../components/elements/CommitmentToQuality";
import React from "react";
import HomeBlog from "../components/elements/HomeBlog";
import OrderModal from "../components/ecommerce/OrderModal";

export default function Home({products}) {
    return (
        <>
            {/*<IntroPopup />*/}

            <Layout noBreadcrumb="d-none">
                <section className="home-slider position-relative mb-30">
                    <div className="container">
                        <div className="home-slide-cover mt-30">
                            <Intro1 />
                        </div>
                    </div>
                </section>

                <section className="product-tabs section-padding position-relative">
                    <div className="container">
                        <div className="col-lg-12">
                            <CategoryTab title="Tất cả sản phẩm" products={[...products, ...products]} />
                        </div>
                    </div>
                </section>

                <section className="product-tabs section-padding position-relative">
                    <div className="container">
                        <div className="col-lg-12">
                            <CategoryTab title="Sản phẩm bán chạy" products={products} isHot />
                        </div>
                    </div>
                </section>

                <CommitmentToQuality />

                {/*<section className="product-tabs section-padding position-relative">*/}
                {/*    <div className="container">*/}
                {/*        <div className="col-lg-12">*/}
                {/*            <HomeBlog/>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</section>*/}


                <QuickView />
                <OrderModal />
            </Layout>
        </>
    );
}

export async function getServerSideProps (context) {
    const response = await axios.get('/products');
    return {
        props: {
            products: response.data.data,
        },
    }
}
