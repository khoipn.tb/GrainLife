import * as Types from '../constants/actionTypes'

export default (state = null , action)=>{
    switch (action.type) {

        case Types.OPEN_ORDER_MODAL :
            return {
                ...action.payload
            }

        case Types.CLOSE_ORDER_MODAL :
            return  null

        default:
            return state
    }
}
