import { combineReducers } from 'redux'
import cart from './cart'
import quickView from './quickView'
import orderModal from './orderModal'

const rootReducer = combineReducers({
    cart,
    quickView,
    orderModal,
})

export default rootReducer
