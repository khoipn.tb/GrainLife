import * as Types from '../constants/actionTypes'

export const openOrderModal = (fullName, phoneNumber, shipAddress) => dispatch =>{
    dispatch({
        type: Types.OPEN_ORDER_MODAL,
        payload: { fullName, phoneNumber, shipAddress }
    })
}

export const closeOrderModal = ()=> dispatch =>{
    dispatch({
        type: Types.CLOSE_ORDER_MODAL,
    })
}
