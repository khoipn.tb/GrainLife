import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.BASE_API,
});

// axiosInstance.interceptors.request.use((config) => {
//   const token = localStorage.getItem('accessToken');
//   config.headers.Authorization =  token || '';
//   return config;
// });

// axiosInstance.interceptors.response.use(
//   (response) => response,
//   (error) => Promise.reject((error.response && error.response.data) || 'Something went wrong')
// );

export default axiosInstance;
