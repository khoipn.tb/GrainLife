export const deleteProduct = (list, id) => {
  return list.filter((item) => item._id !== id);
  };

export const findProductIndex = (list, slug) => {
  return list.findIndex((item) => item.slug === slug);
};

export const findProductIndexById = (list, id) => {
  return list.findIndex((item) => item._id === id);
};
