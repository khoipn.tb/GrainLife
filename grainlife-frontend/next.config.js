module.exports = {
	async rewrites() {
		return [
			{
				source: '/about',
				destination: '/'
			}
		]
	},
	publicRuntimeConfig: {
		BASE_API: process.env.BASE_API
	},
	images: {
		domains: ['cdn.shopify.com', 'res.cloudinary.com']
	}
}
