import React, { useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import {
    addToCart,
    decreaseQuantity,
    increaseQuantity
} from "../../redux/action/cart";
import ProductTab from "../elements/ProductTab";
import ThumbSlider from "../sliders/Thumb";
import {fCurrency} from "../../util/formatNumber";

const ProductDetails = ({
    product,
    addToCart,
    quickView,
}) => {
    const handleCart = (product) => {
        addToCart(product);
        toast("Product added to Cart !");
    };

    return (
        <>
            <section className="mt-50 mb-50">
                <div className="container">
                    <div className="row flex-row-reverse">
                        <div className="col-xl-10 col-lg-12 m-auto">
                            <div className="product-detail accordion-detail">
                                <div className="row mb-50  mt-30">
                                    <div className="col-md-6 col-sm-12 col-xs-12 mb-md-0 mb-sm-5">
                                        <div className="detail-gallery">
                                            <span className="zoom-icon">
                                                <i className="fi-rs-search"></i>
                                            </span>

                                            <div className="product-image-slider">
                                                <ThumbSlider product={product} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-12 col-xs-12">
                                        <div className="detail-info  pr-30 pl-30">
                                            <h2 className="title-detail">{product.name}</h2>
                                            <ul className="font-xs color-grey mt-10">
                                                <li>
                                                    Trạng thái:
                                                    {product.inStock ? (
                                                      <span className="in-stock text-success ml-5">Còn hàng</span>
                                                    ) : (
                                                      <span className="in-stock text-warning ml-5">Tạm hết hàng</span>
                                                    )}
                                                </li>
                                            </ul>
                                            <div className="clearfix product-price-cover">
                                                <div className="product-price primary-color float-left">
                                                    {
                                                        (product.priceSale && product.priceSale !== "0") ? (
                                                          	<>
																																<span className="current-price  text-brand">{fCurrency(product.priceSale)}</span>
                                                              	<span>
                                                              			<span className="old-price font-md ml-15">{fCurrency(product.price)}</span>
                                                    						</span>
																														</>

                                                        ) : (
                                                        		<span className="current-price  text-brand">{fCurrency(product.price)}</span>
                                                        )
                                                    }
                                                </div>
                                            </div>
                                            <div className="bt-1 border-color-1 mt-30 mb-30"></div>
                                            <div className="detail-extralink">
                                                <div className="product-extra-link2">
                                                    <button
                                                        onClick={() =>
                                                            handleCart({
                                                                ...product,
                                                                quantity: 1,
                                                            })
                                                        }
                                                        className="button button-add-to-cart"
                                                    >
                                                        Thêm vào giỏ hàng
                                                    </button>
                                                </div>
                                            </div>
                                            <ul className="font-xs color-grey mt-30">
                                                <li>
                                                    <span>Gọi đặt mua: <a href="tel:0932199969">0932199969</a> để nhanh chóng đặt hàng</span>
                                                </li>
                                                <li>
                                                    Miễn phí vận chuyển cho đơn hàng từ 200,000₫
                                                </li>
                                            </ul>
                                            <div className="iconContainer mt-25">
                                                <div className="blockContainer justify-content-start">
                                                    <img className="featureIcon"
                                                         src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/tree.svg?v=1667157369"
                                                         alt="Tree"/>
                                                        <p className="featureTitle">Nguyên chất từ thiên nhiên</p>
                                                </div>
                                                <div className="blockContainer justify-content-start">
                                                    <img className="featureIcon"
                                                         src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/women.svg?v=1667157366"
                                                         alt="Suc khoe"/>
                                                        <p className="featureTitle">Bổ dưỡng tốt cho sức khỏe</p>
                                                </div>
                                                <div className="blockContainer justify-content-start">
                                                    <img className="featureIcon"
                                                         src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/truck.svg?v=1667157379"
                                                         alt="Truck"/>
                                                        <p className="featureTitle">Giá rẻ xuất trực tiếp từ nông
                                                            trại</p>
                                                </div>
                                                <div className="blockContainer justify-content-start">
                                                    <img className="featureIcon"
                                                         src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/bee.svg?v=1667157360"
                                                         alt="Ong"/>
                                                        <p className="featureTitle">Chứng nhận kiểm định chất lượng</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {quickView ? null : (
                                    <>
                                        <ProductTab product={product} />
                                        {/*<div className="row mt-60">*/}
                                        {/*    <div className="col-12">*/}
                                        {/*        <h3 className="section-title style-1 mb-30">Sản phẩm liên quan</h3>*/}
                                        {/*    </div>*/}
                                        {/*    <div className="col-12">*/}
                                        {/*        <div className="row related-products position-relative">*/}
                                        {/*            <RelatedSlider />*/}
                                        {/*        </div>*/}
                                        {/*    </div>*/}
                                        {/*</div>*/}
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
    addToCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
