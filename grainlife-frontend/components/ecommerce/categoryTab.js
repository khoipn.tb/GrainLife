import React, { useEffect, useState } from "react";
import Cat1Tab from '../elements/FeaturedTab';

function CategoryTab({products, title, isHot}) {

    return (
        <>
            <div className="section-title style-2 wow animate__animated animate__fadeIn">
                <h3 className="d-flex align-items-center">
                  {title}
                  {isHot && <img className="ml-15" width={30} height={30} src="/assets/imgs/theme/icons/icon-hot.svg" alt="Sản phẩm bán chạy" />}
                </h3>
            </div>

            <div
              className="tab-pane fade show active"
            >
                <div className="product-grid-4 row">
                    <Cat1Tab products={products} />
                </div>
            </div>
        </>
    );
}
export default CategoryTab;
