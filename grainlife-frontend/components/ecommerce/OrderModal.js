import React, {useState, useEffect} from "react";
import { connect } from "react-redux";
import { Modal } from 'react-responsive-modal';
import { closeOrderModal } from '../../redux/action/orderModalAction';
import storage from "../../util/localStorage";

const initialFormState = {
  fullName: '', phoneNumber: '', shipAddress: '',
};
const OrderModal = ({ isOpen, closeOrderModal }) => {
  const [values, setValues] = useState(initialFormState);
  const [errors, setErrors] = useState({});

  useEffect(() => {
    setValues(storage.get("shipping_info") || initialFormState);
  }, []);

  const set = name => {
    return ({ target: { value } }) => {
      setValues(oldValues => ({...oldValues, [name]: value }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const { fullName, phoneNumber, shipAddress } = values;

    const errorsValidate = validate(fullName, phoneNumber, shipAddress);
    setErrors(errorsValidate);
    if (Object.keys(errorsValidate).length > 0) {
      console.log('errorsValidate', errorsValidate);
      return;
    }
    storage.set("shipping_info", values);
    console.log(values)
  };
    return (
        <>
            <Modal center open={isOpen} onClose={closeOrderModal}>
              <div className="order-modal pt-25 pb-35 pl-40 pr-40">
                <div className="title d-flex mb-25 mt-25">
                  <h3 className="justify-content-center align-items-center">Hoàn tất đơn hàng</h3>
                </div>
                <form
                  onSubmit={handleSubmit}
                  className="form-contact comment_form"
                >
                  <div className="row">
                    <div className="col-12 d-flex flex-column">
                      <div className="form-group">
                        <label className="mr-10">Tên người nhận <span style={{color: 'red'}}>*</span></label>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Nguyễn Văn A"
                          value={values.fullName}
                          onChange={set('fullName')}
                        />
                        {
                          errors.fullName && (
                            <small className="form-text text-danger">
                              {errors.fullName}
                            </small>
                          )
                        }
                      </div>
                      <div className="form-group">
                        <label className="mr-10">Điện thoại liên hệ <span style={{color: 'red'}}>*</span></label>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="0965873333"
                          value={values.phoneNumber}
                          onChange={set('phoneNumber')}
                        />
                        {
                          errors.phoneNumber && (
                            <small className="form-text text-danger">
                              {errors.phoneNumber}
                            </small>
                          )
                        }
                      </div>
                      <div className="form-group">
                        <label className="mr-10">Địa chỉ nhận hàng <span style={{color: 'red'}}>*</span></label>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="222 đường Mỹ Đình, quận Nam Từ Liêm, Hà Nội"
                          value={values.shipAddress}
                          onChange={set('shipAddress')}
                        />
                        {
                          errors.shipAddress && (
                            <small className="form-text text-danger">
                              {errors.shipAddress}
                            </small>
                          )
                        }
                      </div>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <button
                      type="submit"
                      className="button button-contactForm"
                    >
                      Hoàn tất đơn hàng
                    </button>
                  </div>
                </form>
              </div>
            </Modal>
        </>
    );
};

function validate(fullName, phoneNumber, shipAddress) {
  const errors = {};

  if (fullName.length === 0) {
    errors["fullName"] = "Vui lòng nhập tên người nhận";
  }

  if (phoneNumber.length === 0) {
    errors["phoneNumber"] = "Vui lòng nhập điện thoại liên hệ";
  }

  const isValidPhone = phone => /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/.test(phone);

  if (!isValidPhone(phoneNumber)) {
    errors["phoneNumber"] = "Số điện thoại không hợp lệ";
  }

  if (shipAddress.length === 0) {
    errors["shipAddress"] = "Vui lòng nhập địa chỉ nhận hàng";
  }

  return errors;
}

export default OrderModal;
