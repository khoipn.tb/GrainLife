import Link from "next/link";
import Image from "next/image";
import React from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { addToCart } from "../../redux/action/cart";
import { openQuickView } from "../../redux/action/quickViewAction";
import {fCurrency} from "../../util/formatNumber";

const SingleProduct = ({
    product,
    addToCart,
    openQuickView,
}) => {
    const handleCart = (product) => {
        addToCart(product);
        toast("Đã thêm sản phẩm vào giỏ hàng!");
    };
    return (
        <>
            <div className="product-cart-wrap mb-30">
                <div className="product-img-action-wrap">
                    <div className="product-img product-img-zoom">
                        <Link
                            href="/san-pham/[slug]"
                            as={`/san-pham/${product.slug}`}
                        >
                            <a>
                                <Image
                                    className="default-img"
                                    src={product.images[0]}
                                    alt=""
                                    layout="fill"
                                />
                                {
                                    product.images[1] && (
                                    <Image
                                      className="hover-img"
                                      src={product.images[1]}
                                      alt=""
                                      layout="fill"
                                    />
                                  )
                                }
                            </a>
                        </Link>
                    </div>
                    <div className="product-action-1">
                        <a
                            href="#"
                            aria-label="Xem nhanh"
                            className="action-btn hover-up"
                            data-bs-toggle="modal"
                            onClick={(e) => openQuickView(product)}
                        >
                            <i className="fi-rs-eye"></i>
                        </a>
                    </div>

                    <div className="product-badges product-badges-position product-badges-mrg">
                        {product.trending && <span className="hot">Hot</span>}
                        {product.created && <span className="new">New</span>}
                        {product.totalSell > 100 && (
                            <span className="best">Best Sell</span>
                        )}
                        {/*{product.discount.isActive && (*/}
                        {/*    <span className="sale">Sale</span>*/}
                        {/*)}*/}
                        {/*{product.discount.percentage >= 5 && (*/}
                        {/*    <span className="hot">*/}
                        {/*        {product.discount.percentage}%*/}
                        {/*    </span>*/}
                        {/*)}*/}
                    </div>
                </div>
                <div className="product-content-wrap">
                    <h2 className="mt-15">
                        <Link
                            href="/san-pham/[slug]"
                            as={`/san-pham/${product.slug}`}
                        >
                            <a>{product.name}</a>
                        </Link>
                    </h2>
                    <div className="product-card-bottom">
                        {
                          (product.priceSale && product.priceSale !== "0") ? (
                            <div className="product-price">
                                <span>{fCurrency(product.priceSale)}</span>
                                <span className="old-price">{fCurrency(product.price)}</span>
                            </div>
                          ) : (
                            <div className="product-price">
                                <span>{fCurrency(product.price)}</span>
                            </div>
                          )
                        }
                        <div className="add-cart">
                            <a
                                href="#"
                                className="add"
                                onClick={(e) => handleCart(product)}
                            >
                                <i className="fi-rs-shopping-cart"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

const mapDispatchToProps = {
    addToCart,
    openQuickView,
};

export default connect(null, mapDispatchToProps)(SingleProduct);
