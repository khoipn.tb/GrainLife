import React from "react";
import Link from "next/link"

const Breadcrumb2 = ({title, parent, sub, subChild, noBreadcrumb}) => {

    return (
        <>
            <div className="page-header mt-30 mb-50">
            <div className="container">
                <div className="archive-header">
                    <div className="row align-items-center">
                        <div className="col-xl-6">
                            <h1 className="mb-15 text-capitalize">{title ? title : "Category"}</h1>
                            <div className="breadcrumb">
                                <Link href="/"><a rel="nofollow"><i className="fi-rs-home mr-5"></i>{parent}</a></Link>
                                <span></span> {sub} <span></span> {subChild}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
};

export default Breadcrumb2;
