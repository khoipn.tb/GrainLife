import Link from "next/link";
import { useState } from "react";
import useClickOutside from "../../util/outsideClick";


const MobileMenu = ({ isToggled, toggleClick, categories }) => {
    const [isActive, setIsActive] = useState({
        status: false,
        key: "",
    });

    const handleToggle = (key) => {
        if (isActive.key === key) {
            setIsActive({
                status: false,
            });
        } else {
            setIsActive({
                status: true,
                key,
            });
        }
    };

    let domNode = useClickOutside(() => {
        setIsActive({
            status: false,
        });
    });
    return (
        <>
            <div
                className={
                    isToggled
                        ? "mobile-header-active mobile-header-wrapper-style sidebar-visible"
                        : "mobile-header-active mobile-header-wrapper-style"
                }
            >
                <div className="mobile-header-wrapper-inner">
                    <div className="mobile-header-top">
                        <div className="mobile-header-logo">
                            <Link href="/">
                                <a>
                                    <img
                                        src="/assets/imgs/theme/logo.svg"
                                        alt="logo"
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="mobile-menu-close close-style-wrap close-style-position-inherit">
                            <button
                                className="close-style search-close"
                                onClick={toggleClick}
                            >
                                <i className="icon-top"></i>
                                <i className="icon-bottom"></i>
                            </button>
                        </div>
                    </div>
                    <div className="mobile-header-content-area">
                        <div className="mobile-search search-style-3 mobile-header-border">
                            <form action="#">
                                <input
                                    type="text"
                                    placeholder="Tìm kiếm sản phẩm"
                                />
                                <button type="submit">
                                    <i className="fi-rs-search"></i>
                                </button>
                            </form>
                        </div>
                        <div className="mobile-menu-wrap mobile-header-border">
                            <nav>
                                <ul className="mobile-menu" ref={domNode}>
                                    <li
                                        className={
                                            isActive.key === 1
                                                ? "menu-item-has-children active"
                                                : "menu-item-has-children"
                                        }
                                    >
                                        <Link href="/">
                                            <a>Trang chủ</a>
                                        </Link>
                                    </li>
                                    <li
                                      className={
                                          isActive.key === 2
                                            ? "menu-item-has-children active"
                                            : "menu-item-has-children"
                                      }
                                    >
                                        <Link href="/">
                                            <a>Giới thiệu</a>
                                        </Link>
                                    </li>
                                    <li
                                        className={
                                            isActive.key === 3
                                                ? "menu-item-has-children active"
                                                : "menu-item-has-children"
                                        }
                                    >
                                        <span
                                            className="menu-expand"
                                            onClick={() => handleToggle(2)}
                                        >
                                            <i className="fi-rs-angle-small-down"></i>
                                        </span>
                                        <a href="#">Cửa hàng</a>
                                        <ul
                                            className={
                                                isActive.key == 2
                                                    ? "dropdown"
                                                    : "d-none"
                                            }
                                        >
                                            <li>
                                                <Link href="/danh-muc/[slug]/trang/[trang]" as="/danh-muc/tat-ca-san-pham/trang/1">
                                                    <a>
                                                        Tất cả sản phẩm
                                                    </a>
                                                </Link>
                                            </li>
                                            {
                                                categories?.map((category, idx) => (
                                                  <li key={`${category._id}-${idx}`}>
                                                      <Link href={`/danh-muc/${category.slug}/trang/1`}>
                                                          <a>
                                                              {category.name}
                                                          </a>
                                                      </Link>
                                                  </li>
                                                ))
                                            }
                                        </ul>
                                    </li>
                                    <li
                                      className={
                                          isActive.key == 1
                                            ? "menu-item-has-children active"
                                            : "menu-item-has-children"
                                      }
                                    >
                                        <Link href="/tin-tuc/trang/[trang]" as="/tin-tuc/trang/1">
                                            <a>Tin tức</a>
                                        </Link>
                                    </li>
                                    <li
                                      className={
                                          isActive.key == 1
                                            ? "menu-item-has-children active"
                                            : "menu-item-has-children"
                                      }
                                    >
                                        <Link href="/">
                                            <a>Liên hệ</a>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div className="mobile-social-icon">
                            <h5 className="mb-15 text-grey-4">Theo dõi chúng tôi</h5>
                            <Link href="#">
                                <a>
                                    <img
                                        src="/assets/imgs/theme/icons/icon-facebook.svg"
                                        alt=""
                                    />
                                </a>
                            </Link>
                            <Link href="#">
                                <a>
                                    <img
                                        src="/assets/imgs/theme/icons/icon-twitter.svg"
                                        alt=""
                                    />
                                </a>
                            </Link>
                            <Link href="#">
                                <a>
                                    <img
                                        src="/assets/imgs/theme/icons/icon-instagram.svg"
                                        alt=""
                                    />
                                </a>
                            </Link>
                            <Link href="#">
                                <a>
                                    <img
                                        src="/assets/imgs/theme/icons/icon-pinterest.svg"
                                        alt=""
                                    />
                                </a>
                            </Link>
                            <Link href="#">
                                <a>
                                    <img
                                        src="/assets/imgs/theme/icons/icon-youtube.svg"
                                        alt=""
                                    />
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default MobileMenu;
