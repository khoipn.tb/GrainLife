import React, { useState } from "react";
import Head from "next/head";
import getConfig from 'next/config'
import Breadcrumb from "./Breadcrumb";
import Footer from "./Footer";
import Header from "./Header";
import MobileMenu from "./MobileMenu";
import axios from "../../util/axios";
import useSWR from "swr";

const {publicRuntimeConfig} = getConfig();
const {BASE_API} = publicRuntimeConfig;

const fetcher = async () => {
  const response = await axios.get(`${BASE_API}/categories`);
  return response?.data?.data;
};
const Layout = ({
    children,
    parent,
    sub,
    subChild,
    noBreadcrumb,
    headerStyle,
}) => {
  const { data: categories } = useSWR('/categories', fetcher);
  const [isToggled, setToggled] = useState(false);
    const toggleClick = () => {
        setToggled(!isToggled);
        isToggled
            ? document
                  .querySelector("body")
                  .classList.remove("mobile-menu-active")
            : document
                  .querySelector("body")
                  .classList.add("mobile-menu-active");
    };

    return (
        <>
            <Head>
                <title>GrainLife - Ngũ cốc dinh dưỡng thuần thực vật</title>
                <meta name="description" content="GrainLife chuyên cung cấp các sản phẩm Hạt Mix Cao Cấp, Ngũ Cốc Mẹ Bầu, Siêu Ngũ Cốc Lợi Sữa 6IN1, Ngũ Cốc Strongfile, Bột Mầm Ngũ Cốc Cao Cấp, Bột Ngũ Cốc Rau Củ..." />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            {isToggled && <div className="body-overlay-1" onClick={toggleClick}></div>}

            <Header categories={categories} headerStyle={headerStyle} isToggled={isToggled} toggleClick={toggleClick} />
            <MobileMenu categories={categories} isToggled={isToggled} toggleClick={toggleClick} />
            <main className="main">
                <Breadcrumb parent={parent} sub={sub} subChild={subChild} noBreadcrumb={noBreadcrumb} />
                {children}
            </main>
            <Footer />
        </>
    );
};

export default Layout;
