import React from "react";
import Link from "next/link"

const Footer = () => {
    return (
        <>
            <footer className="main">
                <section className="newsletter pt-25 pb-25 wow animate__animated animate__fadeIn">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="position-relative newsletter-inner">
                                    <div className="newsletter-content">
                                        <h2 className="mb-20">
                                            Đăng kí nhận tin khuyến mãi
                                        </h2>
                                        <p className="mb-45">
                                            Đăng kí ngay để nhận tin khuyến mãi <br/> và tin tức mới nhất từ{' '}
                                            <span className="text-brand">
                                                GrainLife.vn
                                            </span>
                                        </p>
                                        <form className="form-subcriber d-flex">
                                            <input
                                                type="email"
                                                placeholder="Email của bạn"
                                            />
                                            <button className="btn" type="submit">
                                                Đăng ký
                                            </button>
                                        </form>
                                    </div>
                                    <img
                                        src="https://bizweb.dktcdn.net/100/442/861/themes/845237/assets/bg_mail_right.png?1662687385614"
                                        alt="newsletter"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section-padding footer-mid">
                    <div className="container pt-15 pb-20">
                        <div className="row">
                            <div className="col">
                                <div
                                    className="widget-about font-md mb-md-3 mb-lg-3 mb-xl-0  wow animate__animated animate__fadeInUp"
                                    data-wow-delay="0"
                                >
                                    <div className="logo  mb-30">
                                        {/*<Link href="/"><a className="mb-15">*/}
                                        {/*    <img*/}
                                        {/*        src="/assets/imgs/theme/logo.svg"*/}
                                        {/*        alt="logo"*/}
                                        {/*    />*/}
                                        {/*</a>*/}
                                        {/*</Link>*/}
                                        <h4 className="widget-title">Công ty cổ phần dinh dưỡng GrainLife</h4>
                                    </div>
                                    <ul className="contact-infor">
                                        <li>
                                            <img
                                                src="/assets/imgs/theme/icons/icon-location.svg"
                                                alt=""
                                            />
                                            <strong>Địa chỉ: </strong>{" "}
                                            <span>
                                                Mỹ Đình, Nam Từ Liêm, Hà Nội
                                            </span>
                                        </li>
                                        <li>
                                            <img
                                                src="/assets/imgs/theme/icons/icon-contact.svg"
                                                alt=""
                                            />
                                            <strong>Số điện thoại :</strong>
                                            <span>(+84) 932 199 969</span>
                                        </li>
                                        <li>
                                            <img
                                                src="/assets/imgs/theme/icons/icon-email-2.svg"
                                                alt=""
                                            />
                                            <strong>Email:{' '}</strong>
                                            <span>sale@grainlife.vn</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div
                                className="footer-link-widget col  wow animate__animated animate__fadeInUp"
                                data-wow-delay=".1s"
                            >
                                <h4 className="widget-title">Về Chúng Tôi</h4>
                                <ul className="footer-list  mb-sm-5 mb-md-0">
                                    <p>GrainLife là đơn vị chuyên cung các loại sản phẩm từ ngũ cốc có giấy kiểm định chất lượng và vệ sinh an toàn thực phẩm. Cam kết sản phẩm thật - sản phẩm nguyên chất.</p>
                                </ul>
                            </div>
                            <div
                                className="footer-link-widget col  wow animate__animated animate__fadeInUp"
                                data-wow-delay=".2s"
                            >
                                <h4 className="widget-title ">Các chính sách</h4>
                                <ul className="footer-list  mb-sm-5 mb-md-0">
                                    <li>
                                        <a href="#">Chính Sách Bán Hàng</a>
                                    </li>
                                    <li>
                                        <a href="#">Chính Sách Đại Lý</a>
                                    </li>
                                    <li>
                                        <a href="#">Chính sách bảo mật thông tin</a>
                                    </li>
                                    <li>
                                        <a href="#">Chính sách đổi trả</a>
                                    </li>
                                    <li>
                                        <a href="#">Chính sách giao hàng</a>
                                    </li>
                                    <li>
                                        <a href="#">Shipping Details</a>
                                    </li>
                                    <li>
                                        <a href="#">Compare products</a>
                                    </li>
                                </ul>
                            </div>
                            <div
                                className="footer-link-widget widget-install-app col  wow animate__animated animate__fadeInUp"
                                data-wow-delay=".5s"
                            >
                                <h4 className="widget-title ">Thanh toán an toàn</h4>
                                <img
                                    className=""
                                    src="/assets/imgs/theme/payment-method.png"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                </section>
                <div
                    className="container pb-30  wow animate__animated animate__fadeInUp"
                    data-wow-delay="0"
                >
                    <div className="row align-items-center">
                        <div className="col-12 mb-30">
                            <div className="footer-bottom"></div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6">
                            <p className="font-sm mb-0">
                                &copy; 2022, GrainLife
                            </p>
                        </div>
                        <div className="col-xl-4 col-lg-6 text-center d-none d-xl-block">
                            <div className="hotline d-lg-inline-flex mr-30">
                                <img
                                    src="/assets/imgs/theme/icons/phone-call.svg"
                                    alt="hotline"
                                />
                                <p>
                                    0932 199 969<span>Làm việc 8:00 - 22:00</span>
                                </p>
                            </div>
                            <div className="hotline d-lg-inline-flex">
                                <img
                                    src="/assets/imgs/theme/icons/phone-call.svg"
                                    alt="hotline"
                                />
                                <p>
                                    0932 199 969<span>24/7 Trung tâm hỗ trợ</span>
                                </p>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 text-end d-none d-md-block">
                            <div className="mobile-social-icon">
                                <h6>Follow Us</h6>
                                <a href="#">
                                    <img
                                        src="/assets/imgs/theme/icons/icon-facebook-white.svg"
                                        alt=""
                                    />
                                </a>
                                <a href="#">
                                    <img
                                        src="/assets/imgs/theme/icons/icon-twitter-white.svg"
                                        alt=""
                                    />
                                </a>
                                <a href="#">
                                    <img
                                        src="/assets/imgs/theme/icons/icon-instagram-white.svg"
                                        alt=""
                                    />
                                </a>
                                <a href="#">
                                    <img
                                        src="/assets/imgs/theme/icons/icon-pinterest-white.svg"
                                        alt=""
                                    />
                                </a>
                                <a href="#">
                                    <img
                                        src="/assets/imgs/theme/icons/icon-youtube-white.svg"
                                        alt=""
                                    />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
};

export default Footer;
