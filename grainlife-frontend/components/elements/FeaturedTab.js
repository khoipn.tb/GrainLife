import SingleProduct from "../ecommerce/SingleProduct";

const FeaturedTab = ({ products }) => {
    return (
        <>
            {products.map((product, idx) => (
                <div key={`${product._id}-${idx}`} className="col-lg-1-5 col-md-4 col-12 col-sm-6">
                    <SingleProduct product={product} />
                </div>
            ))}
        </>
    );
};

export default FeaturedTab;
