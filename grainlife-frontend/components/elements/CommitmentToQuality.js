import React from 'react';
import Image from 'next/image';

const CommitmentToQuality = () => {
	return (
		<section className="featured  section-padding">
			<div className="container">
				<div className="row">
					<div className="col-lg-3 col-12">
						<div
							className="banner-left-icon d-flex flex-column justify-content-center align-items-center  wow animate__animated animate__fadeInUp"
							data-wow-delay=".1s"
						>
							<div className="banner-icon">
								<Image
									width={80}
									height={80}
									src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/1vector-logos-of-green-tree-leaf-ecology_1420x.png?v=1627278903"
									alt=""
								/>
							</div>
							<div className="banner-text d-flex flex-column justify-content-center align-items-center pl-25 pr-25">
								<h3 className="icon-box-title">
									Nguyên chất
								</h3>
								<p className="text-center">100% nguyên chất từ thiên nhiên, không pha tạp hóa chất</p>
							</div>
						</div>
					</div>
					<div className="col-lg-3 col-12">
						<div
							className="banner-left-icon d-flex flex-column justify-content-center align-items-center  wow animate__animated animate__fadeInUp"
							data-wow-delay=".2s"
						>
							<div className="banner-icon">
								<Image
									width={80}
									height={80}
									src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/1commitment-glyph-flat-icon-commitment-icon-elements-mobile-concept-web-apps-thin-line-icons-website-design-152001960.png?v=1627295264"
									alt=""
								/>
							</div>
							<div className="banner-text d-flex flex-column justify-content-center align-items-center pl-25 pr-25">
								<h3 className="icon-box-title">
									An Toàn
								</h3>
								<p className="text-center">Đã được kiểm định chất lượng và vệ sinh an toàn thực phẩm</p>
							</div>
						</div>
					</div>
					<div className="col-lg-3 col-12">
						<div
							className="banner-left-icon d-flex flex-column justify-content-center align-items-center  wow animate__animated animate__fadeInUp"
							style={{
								flexDirection: 'column',
							}}
							data-wow-delay=".3s"
						>
							<div className="banner-icon">
								<Image
									width={80}
									height={80}
									src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/111rapeseed-icon-vector-22525510.png?v=1627282405"
									alt=""
								/>
							</div>
							<div className="banner-text d-flex flex-column justify-content-center align-items-center pl-25 pr-25">
								<h3 className="icon-box-title">
									Giá tốt
								</h3>
								<p className="text-center">Bán trực tiếp từ nông trại với giá tốt nhất thị trường, hỗ trợ 24/7</p>
							</div>
						</div>
					</div>
					<div className="col-lg-3 col-12">
						<div
							className="banner-left-icon d-flex flex-column justify-content-center align-items-center  wow animate__animated animate__fadeInUp"
							data-wow-delay=".4s"
						>
							<div className="banner-icon">
								<Image
									width={80}
									height={80}
									src="https://cdn.shopify.com/s/files/1/0586/6268/2802/files/1healthy-life-logo-template-vector-icon-vector_1420x.png?v=1627294779"
									alt=""
								/>
							</div>
							<div className="banner-text d-flex flex-column justify-content-center align-items-center pl-25 pr-25">
								<h3 className="icon-box-title">
									Bổ dưỡng
								</h3>
								<p className="text-center">Bổ sung dưỡng chất tăng cường đề kháng, tốt cho sức khỏe</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default CommitmentToQuality;
