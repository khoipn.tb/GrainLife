
import Link from "next/link";

const BlogGrid = ({ blogs, wide }) => {
    return (
        <>
            {blogs?.map((item, i) => (
                <article
                    className={
                        wide
                            ? "col-xl-3 col-lg-4 col-md-6 text-center hover-up mb-30 animated"
                            : "col-xl-4 col-lg-6 col-md-6 text-center hover-up mb-30 animated"
                    }
                    key={`${item._id}-${i}`}
                >
                    <div className="post-thumb">
                        <Link href="/tin-tuc/[slug]" as={`/tin-tuc/${item.slug}`}>
                            <a>
                                <img
                                    className="border-radius-15"
                                    src={item.cover}
                                    alt=""
                                />
                            </a>
                        </Link>
                    </div>
                    <div className="entry-content-2">
                        <h4 className="post-title mb-15">
                            <Link href="/tin-tuc/[slug]" as={`/tin-tuc/${item.slug}`}>
                                <a>{item.title}</a>
                            </Link>
                        </h4>
                        <div className="entry-meta font-xs color-grey mt-10 pb-10">
                            <div>
                                <span className="post-on mr-10">{(new Date(item.createdAt)).toLocaleDateString('vi-VN')}</span>
                                <span className="hit-count has-dot mr-10">
                                    Đăng bởi {item.author.email}
                                </span>
                            </div>
                        </div>
                    </div>
                </article>
            ))}
        </>
    );
};

export default BlogGrid;
