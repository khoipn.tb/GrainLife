import Link from "next/link";
import React from "react";

const BlogSingle = ({blog}) => {
    return (
        <>
            <div className="single-page pt-50 pr-30">
                <div className="single-header style-2">
                    <div className="row">
                        <div className="col-xl-10 col-lg-12 m-auto">
                            <h2 className="mb-10">
                                {blog?.title}
                            </h2>
                            <div className="single-header-meta">
                                <div className="entry-meta meta-1 font-xs mt-15 mb-15">
                                    <span className="post-by">
                                        Đăng bởi {blog?.author?.email}
                                    </span>
                                    <span className="post-on has-dot">
                                        {(new Date(blog.createdAt)).toLocaleDateString('vi-VN')}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <figure className="single-thumbnail d-flex justify-content-center align-items-center">
                    <img src={blog?.cover} alt="" />
                </figure>
                <div className="single-content">
                    <div className="row">
                        <div className="col-xl-10 col-lg-12 m-auto">
                            <div className="" dangerouslySetInnerHTML={{__html: blog.content}}>
                            </div>
                            <div className="entry-bottom mt-50 mb-30">
                                <div className="social-icons single-share">
                                    <ul className="text-grey-5 d-inline-block">
                                        <li>
                                            <strong className="mr-10">
                                                Chia sẻ bài viết:
                                            </strong>
                                        </li>
                                        <li className="social-facebook">
                                            <Link href="#"><a>
                                                <img
                                                    src="/assets/imgs/theme/icons/icon-facebook.svg"
                                                    alt=""
                                                />
                                            </a></Link>
                                        </li>
                                        <li className="social-twitter">
                                            <Link href="#"><a>
                                                <img
                                                    src="/assets/imgs/theme/icons/icon-twitter.svg"
                                                    alt=""
                                                />
                                            </a></Link>
                                        </li>
                                        <li className="social-instagram">
                                            <Link href="#"><a>
                                                <img
                                                    src="/assets/imgs/theme/icons/icon-instagram.svg"
                                                    alt=""
                                                />
                                            </a></Link>
                                        </li>
                                        <li className="social-linkedin">
                                            <Link href="#"><a>
                                                <img
                                                    src="/assets/imgs/theme/icons/icon-pinterest.svg"
                                                    alt=""
                                                />
                                            </a></Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="comment-form">
                                <h3 className="mb-15">Leave a Comment</h3>
                                <div className="product-rate d-inline-block mb-30"></div>
                                <div className="row">
                                    <div className="col-lg-9 col-md-12">
                                        <form
                                            className="form-contact comment_form mb-50"
                                            action="#"
                                            id="commentForm"
                                        >
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="form-group">
                                                        <textarea
                                                            className="form-control w-100"
                                                            name="comment"
                                                            id="comment"
                                                            cols="30"
                                                            rows="9"
                                                            placeholder="Write Comment"
                                                        ></textarea>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6">
                                                    <div className="form-group">
                                                        <input
                                                            className="form-control"
                                                            name="name"
                                                            id="name"
                                                            type="text"
                                                            placeholder="Name"
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-sm-6">
                                                    <div className="form-group">
                                                        <input
                                                            className="form-control"
                                                            name="email"
                                                            id="email"
                                                            type="email"
                                                            placeholder="Email"
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-12">
                                                    <div className="form-group">
                                                        <input
                                                            className="form-control"
                                                            name="website"
                                                            id="website"
                                                            type="text"
                                                            placeholder="Website"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <button
                                                    type="submit"
                                                    className="button button-contactForm"
                                                >
                                                    Post Comment
                                                </button>
                                            </div>
                                        </form>
                                        <div className="comments-area">
                                            <h3 className="mb-30">Comments</h3>
                                            <div className="comment-list">
                                                <div className="single-comment justify-content-between d-flex mb-30">
                                                    <div className="user justify-content-between d-flex">
                                                        <div className="thumb text-center">
                                                            <img
                                                                src="/assets/imgs/blog/author-2.png"
                                                                alt=""
                                                            />
                                                            <Link href="#"><a

                                                                className="font-heading text-brand"
                                                            >
                                                                Sienna
                                                            </a></Link>
                                                        </div>
                                                        <div className="desc">
                                                            <div className="d-flex justify-content-between mb-10">
                                                                <div className="d-flex align-items-center">
                                                                    <span className="font-xs text-muted">
                                                                        December
                                                                        4, 2021
                                                                        at 3:12
                                                                        pm{" "}
                                                                    </span>
                                                                </div>
                                                                <div className="product-rate d-inline-block">
                                                                    <div
                                                                        className="product-rating"
                                                                        style={{ "width": "80%" }}
                                                                    ></div>
                                                                </div>
                                                            </div>
                                                            <p className="mb-10">
                                                                Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipisicing
                                                                elit. Delectus,
                                                                suscipit
                                                                exercitationem
                                                                accusantium
                                                                obcaecati quos
                                                                voluptate
                                                                nesciunt facilis
                                                                itaque modi
                                                                commodi
                                                                dignissimos
                                                                sequi
                                                                repudiandae
                                                                minus ab
                                                                deleniti totam
                                                                officia id
                                                                incidunt?{" "}
                                                                <Link href="#"><a

                                                                    className="reply"
                                                                >
                                                                    Reply
                                                                </a></Link>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="single-comment justify-content-between d-flex mb-30 ml-30">
                                                    <div className="user justify-content-between d-flex">
                                                        <div className="thumb text-center">
                                                            <img
                                                                src="/assets/imgs/blog/author-3.png"
                                                                alt=""
                                                            />
                                                            <Link href="#"><a

                                                                className="font-heading text-brand"
                                                            >
                                                                Brenna
                                                            </a></Link>
                                                        </div>
                                                        <div className="desc">
                                                            <div className="d-flex justify-content-between mb-10">
                                                                <div className="d-flex align-items-center">
                                                                    <span className="font-xs text-muted">
                                                                        December
                                                                        4, 2021
                                                                        at 3:12
                                                                        pm{" "}
                                                                    </span>
                                                                </div>
                                                                <div className="product-rate d-inline-block">
                                                                    <div
                                                                        className="product-rating"
                                                                        style={{ "width": "80%" }}
                                                                    ></div>
                                                                </div>
                                                            </div>
                                                            <p className="mb-10">
                                                                Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipisicing
                                                                elit. Delectus,
                                                                suscipit
                                                                exercitationem
                                                                accusantium
                                                                obcaecati quos
                                                                voluptate
                                                                nesciunt facilis
                                                                itaque modi
                                                                commodi
                                                                dignissimos
                                                                sequi
                                                                repudiandae
                                                                minus ab
                                                                deleniti totam
                                                                officia id
                                                                incidunt?{" "}
                                                                <Link href="#"><a

                                                                    className="reply"
                                                                >
                                                                    Reply
                                                                </a></Link>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="single-comment justify-content-between d-flex">
                                                    <div className="user justify-content-between d-flex">
                                                        <div className="thumb text-center">
                                                            <img
                                                                src="/assets/imgs/blog/author-4.png"
                                                                alt=""
                                                            />
                                                            <Link href="#"><a

                                                                className="font-heading text-brand"
                                                            >
                                                                Gemma
                                                            </a></Link>
                                                        </div>
                                                        <div className="desc">
                                                            <div className="d-flex justify-content-between mb-10">
                                                                <div className="d-flex align-items-center">
                                                                    <span className="font-xs text-muted">
                                                                        December
                                                                        4, 2021
                                                                        at 3:12
                                                                        pm{" "}
                                                                    </span>
                                                                </div>
                                                                <div className="product-rate d-inline-block">
                                                                    <div
                                                                        className="product-rating"
                                                                        style={{ "width": "80%" }}
                                                                    ></div>
                                                                </div>
                                                            </div>
                                                            <p className="mb-10">
                                                                Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipisicing
                                                                elit. Delectus,
                                                                suscipit
                                                                exercitationem
                                                                accusantium
                                                                obcaecati quos
                                                                voluptate
                                                                nesciunt facilis
                                                                itaque modi
                                                                commodi
                                                                dignissimos
                                                                sequi
                                                                repudiandae
                                                                minus ab
                                                                deleniti totam
                                                                officia id
                                                                incidunt?{" "}
                                                                <Link href="#"><a

                                                                    className="reply"
                                                                >
                                                                    Reply
                                                                </a></Link>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default BlogSingle;
