const Joi = require("joi");

const createProductValidation = {
  body: Joi.object()
    .required()
    .keys({
      name: Joi.string().required(),
      description: Joi.string().required(),
      price: Joi.number().required(),
      priceSale: Joi.number(),
      inStock: Joi.bool(),
      images: Joi.any(),
      category: Joi.string().required(),
      isActive: Joi.bool().required(),
    }),
};

const updateProductValidation = {
  body: Joi.object()
    .required()
    .keys({
      name: Joi.string().required(),
      description: Joi.string().required(),
      price: Joi.number().required(),
      priceSale: Joi.number(),
      inStock: Joi.bool(),
      images: Joi.any(),
      category: Joi.string().required(),
      isActive: Joi.bool().required(),
    }),
};

module.exports = {
  createProductValidation,
  updateProductValidation,
};
