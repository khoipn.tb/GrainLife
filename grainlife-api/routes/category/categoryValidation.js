const Joi = require("joi");

const createCategoryValidation = {
  body: Joi.object().required().keys({
    name: Joi.string().required().messages({
      "any.required": "Tên là bắt buộc",
    }),
    description: Joi.string(),
    isActive: Joi.bool()
  }),
};

const updateCategoryValidation = {
  body: Joi.object().required().keys({
    name: Joi.string().required().messages({
      "any.required": "Tên là bắt buộc",
    }),
    description: Joi.string(),
    isActive: Joi.bool()
  }),
};

module.exports = {
  createCategoryValidation,
  updateCategoryValidation,
};
