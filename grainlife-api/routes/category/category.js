const router = require("express").Router();
const categoryController = require("../../controllers/categoryController");
const productController = require("../../controllers/productController");
const { auth, roles } = require("../../middlewares/auth");
const validation = require("../../middlewares/validation");
const validators = require("./categoryValidation");
const multer = require("multer");

// Route ==> /api/v1/categories/:categoryId/subcategories
// router.use("/:categoryId/subcategories", subcategoryRouter);

// Create new category
router.post(
  "/",
    auth(roles.admin),
		multer().none(),
    validation(validators.createCategoryValidation),
  categoryController.createCategory
);

// Update category
router.patch(
  "/:categoryId",
		multer().none(),
    auth(roles.admin),
    validation(validators.updateCategoryValidation),
  categoryController.updateCategory
);

// Delete category
router.delete(
  "/:categoryId",
  auth(roles.admin),
  categoryController.deleteCategory
);

// Get category
router.get("/:categoryId", categoryController.getCategory);

router.get("/:categoryId/products", productController.getProductsByCategoryId);

// Get all categories
router.get("/", categoryController.getAllCategories);

module.exports = router;
