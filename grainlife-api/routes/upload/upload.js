const router = require("express").Router();
const uploadController = require("../../controllers/uploadController");
const { auth, roles } = require("../../middlewares/auth");
const {upload, fileValidation} = require("../../utils/multer");
const multerErrorHandler = require("../../middlewares/multerErrorHandler");

// Upload file
router.post(
    "/",
    auth([roles.admin, roles.seller, roles.user]),
    upload("quill-image-embed", fileValidation.image).single("embed"),
    multerErrorHandler,
    uploadController.upload
);

module.exports = router;
