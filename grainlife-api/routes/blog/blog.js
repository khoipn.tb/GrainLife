const router = require("express").Router({ mergeParams: true });
const blogController = require("../../controllers/blogController");
const { auth, roles } = require("../../middlewares/auth");
const multerErrorHandler = require("../../middlewares/multerErrorHandler");
const validation = require("../../middlewares/validation");
const validators = require("./blogValidation");
const { upload, fileValidation } = require("../../utils/multer");
const reviewRouter = require("../review/review");

router.use("/:productId/comments", reviewRouter);

router.post(
  "/",
  auth([roles.admin, roles.seller]),
	upload("blogs/covers", fileValidation.image).single("cover"),
	multerErrorHandler,
	validation(validators.createBlogValidation),
	blogController.createBlog
);

router.patch(
  "/:blogId",
	auth([roles.admin, roles.seller]),
	upload("blogs/covers", fileValidation.image).single("cover"),
	multerErrorHandler,
  validation(validators.updateBlogValidation),
  blogController.updateBlog
);

router.delete(
  "/:blogId",
  auth([roles.admin, roles.seller]),
  blogController.deleteBlog
);

router.get("/:blogId", blogController.getBlog);

router.get("/", blogController.getAllBlogs);

module.exports = router;
