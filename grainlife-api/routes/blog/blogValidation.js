const Joi = require("joi");

const createBlogValidation = {
  body: Joi.object()
    .required()
    .keys({
      title: Joi.string().required(),
      content: Joi.string().required(),
      cover: Joi.binary(),
      metaTitle: Joi.string().allow(null, ''),
      metaDescription: Joi.string().allow(null, ''),
      metaKeywords: Joi.string().allow(null, ''),
      enableComment: Joi.bool().required(),
      isActive: Joi.bool().required(),
    }),
};

const updateBlogValidation = {
  body: Joi.object()
    .required()
    .keys({
      title: Joi.string().required(),
      content: Joi.string().required(),
      cover: Joi.binary(),
      metaTitle: Joi.string().allow(null, ''),
      metaDescription: Joi.string().allow(null, ''),
      metaKeywords: Joi.string().allow(null, ''),
      enableComment: Joi.bool().required(),
      isActive: Joi.bool().required(),
    }),
};

module.exports = {
  createBlogValidation,
  updateBlogValidation,
};
