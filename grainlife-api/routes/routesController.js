const authRouter = require("./auth/auth");
const categoryRouter = require("./category/category");
const orderRouter = require("./order/order");
const productRouter = require("./product/product");
const reviewRouter = require("./review/review");
const userRouter = require("./user/user");
const uploadRouter = require("./upload/upload");
const blogRouter = require("./blog/blog");

module.exports = {
  authRouter,
  categoryRouter,
  orderRouter,
  productRouter,
  reviewRouter,
  userRouter,
  uploadRouter,
  blogRouter,
};
