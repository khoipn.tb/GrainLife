echo '################ MONGO ENTRYPOINT START ################';

mongo -- "$MONGO_INITDB_DATABASE" <<EOF
db = db.getSiblingDB('$MONGODB_DATABASE');
db.createUser(
  {
    user: '$MONGODB_USER',
    pwd: '$MONGODB_PASSWORD',
    roles: [{ role: 'readWrite', db: '$MONGODB_DATABASE' }],
  },
);
db.createCollection('users');
EOF
