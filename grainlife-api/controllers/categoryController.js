const Category = require("../models/Category");
const AppError = require("../utils/appError");
const slugify = require("slugify");
const APIFeatures = require("../utils/apiFeatures");
const Product = require("../models/Product");
const {isValidObjectId, Types} = require("mongoose");

// @desc    Create new category
// @route   POST /api/v1/categories
// @access  Private
const createCategory = async (req, res, next) => {
  try {
    const newCategory = new Category(req.body);
    const savedCategory = await newCategory.save();
    res.status(201).json({
      status: "success",
      message: "Tạo danh mục thành công",
      data: savedCategory,
    });
  } catch (error) {
    console.log(error.message);
    if (error.keyValue?.name) {
      return next(new AppError("Danh mục có tên này đã tồn tại", 400));
    } else {
      return next(new AppError(error.message, 500));
    }
  }
};

// @desc    Update category
// @route   PATCH /api/v1/categories/:categoryId
// @access  Private
const updateCategory = async (req, res, next) => {
  try {
    const category = await Category.findById(req.params.categoryId);
    if (!category) {
      return next(new AppError("Danh mục này không tồn tại", 404));
    }
    // req.body.slug = slugify(req.body.name, { lower: true });
    const updatedCategory = await Category.findByIdAndUpdate(
      req.params.categoryId,
      { $set: req.body },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Cập nhật danh mục thành công",
      data: updatedCategory,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Delete category
// @route   DELETE /api/v1/categories/:categoryId
// @access  Private
const deleteCategory = async (req, res, next) => {
  try {
    const category = await Category.findById(req.params.categoryId);
    if (!category) {
      return next(new AppError("Danh mục này không tồn tại", 404));
    }
    const hasRef = await Product.exists({category: req.params.categoryId});

    if (hasRef) {
      return next(new AppError("Danh mục này tồn tại sản phẩm bên trong", 404));
    }
    await Category.findByIdAndDelete(
      req.params.categoryId
    );
    res.status(200).json({
      status: "Success",
      message: "Xóa danh mục thành công",
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get category
// @route   GET /api/v1/categories/:categoryId
// @access  Public
const getCategory = async (req, res, next) => {
  try {
    const category = await Category.findOne({
      $or: [{ _id: isValidObjectId(req.params.categoryId) ? Types.ObjectId(req.params.categoryId) : undefined }, { slug: req.params.categoryId }]
    });
    if (!category) {
      return next(new AppError("Category not found", 404));
    }
    const products = await Product.find({
      category: category._id,
    });
    res.status(200).json({
      status: "Success",
      data: {
        ...category._doc,
        products
      },
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get all categories
// @route   GET /api/v1/categories/
// @access  Private
const getAllCategories = async (req, res, next) => {
  try {
    const resultsPerPage = Number(process.env.RESULTS_PER_PAGE);
    const apiFeatures = new APIFeatures(Category.find(), req.query)
      // .search()
      // .sort()
      // .filter()
      // .limitFields()
      // .pagination(resultsPerPage);
    const categories = await apiFeatures.query;
    res.status(200).json({
      status: "Success",
      data: categories,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

module.exports = {
  createCategory,
  updateCategory,
  deleteCategory,
  getCategory,
  getAllCategories,
};
