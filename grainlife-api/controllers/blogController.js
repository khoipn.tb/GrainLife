const AppError = require("../utils/appError");
const {uploadFile, destroyFile} = require("../config/cloudinary");
const fs = require("fs");
const {isValidObjectId, Types} = require("mongoose");
const Blog = require("../models/Blog");

const createBlog = async (req, res, next) => {
  try {
    if (req.fileErr) {
      return next(new AppError("Định dạng hình ảnh không hợp lệ"));
    }

    if (req.file) {
      // Upload images to cloudinary
      const uploaded = await uploadFile(req.file.path, "blogs");
      fs.unlinkSync(req.file.path);

      req.body.cover = uploaded.secure_url;
      req.body.coverId = uploaded.public_id;
    }
    req.body.author = req.user._id;

    const newBlog = new Blog(req.body);
    const savedBlog = await newBlog.save();

    res.status(201).json({
      status: "success",
      message: "Thêm bài viết thành công",
      data: savedBlog,
    });
  } catch (error) {
    console.log(error);
    return next(new AppError(error.message, 500));
  }
};

const updateBlog = async (req, res, next) => {
  try {
    const blog = await Blog.findById(req.params.blogId);
    if (!blog) {
      return next(new AppError("Tin tức này không tồn tại", 404));
    }

    if (req.fileErr) {
      return next(new AppError("Định dạng hình ảnh không hợp lệ"));
    }

    if (req.file) {
      // Upload images to cloudinary
      const uploaded = await uploadFile(req.file.path, "blogs");
      fs.unlinkSync(req.file.path);
      req.body.cover = uploaded.secure_url;
      req.body.coverId = uploaded.public_id;
      destroyFile(blog.coverId);
    }

    const updatedBlog = await Blog.findByIdAndUpdate(
      req.params.blogId,
      { $set: req.body },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Cập nhật tin tức thành công",
      data: updatedBlog,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

const deleteBlog = async (req, res, next) => {
  try {
    const blog = await Blog.findById(req.params.blogId);
    if (!blog) {
      return next(new AppError("Tin tức này không tồn tại", 404));
    }
    await Blog.findByIdAndDelete(
      req.params.blogId
    );
    destroyFile(blog.coverId);

    res.status(200).json({
      status: "Success",
      message: "Xóa tin tức thành công",
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

const getBlog = async (req, res, next) => {
  try {
    const blog = await Blog.findOne({
      $or: [{ _id: isValidObjectId(req.params.blogId) ? Types.ObjectId(req.params.blogId) : undefined }, { slug: req.params.blogId }]
    }).populate('author');
    if (!blog) {
      return next(new AppError("Product not found", 404));
    }
    res.status(200).json({
      status: "Success",
      data: blog,
    });
  } catch (error) {
    console.log(error);
    return next(new AppError(error.message, 500));
  }
};

const getAllBlogs = async (req, res, next) => {
  try {
    let { page = 1, limit = 9 } = req.query;
    limit = Number(limit);
    page = Number(page);
    const blogs = await Blog.find()
      .limit(limit)
      .skip((page - 1) * limit)
      .populate('author');
    const count = await Blog.countDocuments();

    res.status(200).json({
      status: "Success",
      data: blogs,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
      limit: limit,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

module.exports = {
  createBlog,
  updateBlog,
  deleteBlog,
  getBlog,
  getAllBlogs,
};
