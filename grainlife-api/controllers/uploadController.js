const AppError = require("../utils/appError");
const {uploadFile} = require("../config/cloudinary");

const upload = async (req, res, next) => {
    try {
        if (req.fileErr) {
            return next(new AppError("Invalid image format"));
        }
        const imageResult = await uploadFile(req.file.path, 'embed');
        const imageURL = imageResult.secure_url;
        res.status(201).json({ url: imageURL });
    } catch (error) {
        return next(new AppError(error.message, 500));
    }
};
module.exports = {
    upload,
};
