const Product = require("../models/Product");
const Category = require("../models/Category");
const Review = require("../models/Review");
const User = require("../models/User");
const APIFeatures = require("../utils/apiFeatures");
const AppError = require("../utils/appError");
const {uploadFile, destroyFile} = require("../config/cloudinary");
const fs = require("fs");
const {isValidObjectId, Types} = require("mongoose");
const Blog = require("../models/Blog");

// @desc    Create new product
// @route   POST /api/v1/products/ <==========> /api/v1/subcategories/:subcategoryId/products
// @access  Private
const createProduct = async (req, res, next) => {
  try {
    if (req.fileErr) {
      return next(new AppError("Định dạng hình ảnh không hợp lệ"));
    }
    const category = await Category.findById(req.body.category);
    if (!category) {
      return next(new AppError("Danh mục này không tồn tại", 404));
    }

    // Upload images to cloudinary
    const folderName = `products`;
    const imagesLink = [];
    const imagesId = [];
    let temp = null;

    for (const file of req.files) {
      temp = await uploadFile(file.path, folderName);
      imagesLink.push(temp.secure_url);
      imagesId.push(temp.public_id);
      fs.unlinkSync(file.path);
    }

    req.body.images = imagesLink;
    req.body.imagesId = imagesId;

    const newProduct = new Product(req.body);
    const savedProduct = await newProduct.save();

    res.status(201).json({
      status: "success",
      message: "Thêm sản phẩm thành công",
      data: savedProduct,
    });
  } catch (error) {
    console.log(error);
    return next(new AppError(error.message, 500));
  }
};

// @desc    Update existing product
// @route   PATCH /api/v1/products/:productId <==========> /api/v1/subcategories/:subcategoryId/products/:productId
// @access  Private
const updateProduct = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Sản phẩm này không tồn tại", 404));
    }

    if (req.fileErr) {
      return next(new AppError("Định dạng hình ảnh không hợp lệ"));
    }
    const category = await Category.findById(req.body.category);
    if (!category) {
      return next(new AppError("Danh mục này không tồn tại", 404));
    }

    if (req.files.length > 0) {
      // Upload images to cloudinary
      const folderName = `products`;
      const imagesLink = [];
      const imagesId = [];
      let temp = null;

      for (const file of req.files) {
        temp = await uploadFile(file.path, folderName);
        imagesLink.push(temp.secure_url);
        imagesId.push(temp.public_id);
        fs.unlinkSync(file.path);
      }

      req.body.images = imagesLink;
      req.body.imagesId = imagesId;
    }

    const updatedProduct = await Product.findByIdAndUpdate(
      req.params.productId,
      { $set: req.body },
      { new: true }
    );

    if (req.files.length > 0) {
      product.imagesId.forEach((imageId) => destroyFile(imageId));
    }

    res.status(200).json({
      status: "Success",
      message: "Cập nhật sản phẩm thành công",
      data: updatedProduct,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Delete existing product
// @route   DELETE  /api/v1/products/:productId <==========> /api/v1/subcategories/:subcategoryId/products/:productId
// @access  Private
const deleteProduct = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Sản phẩm này không tồn tại", 404));
    }
    await Product.findByIdAndDelete(
      req.params.productId
    );
    product.imagesId.forEach((imageId) => destroyFile(imageId));
    // const deletedReviews = await Review.deleteMany({ product: product._id });
    res.status(200).json({
      status: "Success",
      message: "Xóa sản phẩm thành công",
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get product
// @route   GET /api/v1/products/:productId <==========> /api/v1/subcategories/:subcategoryId/products/:productId
// @access  Public
const getProduct = async (req, res, next) => {
  try {
    const product = await Product.findOne({
      $or: [{ _id: isValidObjectId(req.params.slug) ? Types.ObjectId(req.params.slug) : undefined }, { slug: req.params.slug }]
    });
    if (!product) {
      return next(new AppError("Product not found", 404));
    }
    res.status(200).json({
      status: "Success",
      data: product,
    });
  } catch (error) {
    console.log(error);
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get all products
// @route   GET /api/v1/products/ <==========> /api/v1/subcategories/:subcategoryId/products/
// @access  Public
const getAllProducts = async (req, res, next) => {
  try {
    let { page = 1, limit = 9 } = req.query;
    limit = Number(limit);
    page = Number(page);
    const products = await Product.find()
      .limit(limit)
      .skip((page - 1) * limit)
    const count = await Product.countDocuments();

    res.status(200).json({
      status: "Success",
      data: products,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
      limit: limit,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

const getProductsByCategoryId = async (req, res, next) => {
  try {
    let { page = 1, limit = 9 } = req.query;

    const category = await Category.findOne({
      $or: [{ _id: isValidObjectId(req.params.categoryId) ? Types.ObjectId(req.params.categoryId) : undefined }, { slug: req.params.categoryId }]
    });

    if (!category) {
      return next(new AppError("Category not found", 404));
    }

    limit = Number(limit);
    page = Number(page);
    const products = await Product.find({category: category._id})
      .limit(limit)
      .skip((page - 1) * limit)
    const count = await Product.find({category: category._id}).countDocuments();

    res.status(200).json({
      status: "Success",
      data: products,
      category,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
      limit: limit,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get all products by a seller
// @route   GET /api/v1/products/seller/:sellerId
// @access  Public
const getProductsBySeller = async (req, res, next) => {
  try {
    const sellerId = req.params.sellerId;
    const resultPerPage = Number(process.env.RESULTS_PER_PAGE);
    const apiFeatures = new APIFeatures(
      Product.find({ seller: sellerId }),
      req.query
    )
      .search()
      .sort()
      .filter()
      .limitFields()
      .pagination(resultPerPage);
    const products = await apiFeatures.query;
    res.status(200).json({
      status: "Success",
      data: products,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Upload product's cover image
// @route   PATCH /api/v1/products/:productId/upload/cover <=> /api/v1/subcategories/:subcategoryId/products/:productId/upload/cover
// @access  Private
const uploadCoverImg = async (req, res, next) => {
  try {
    if (req.fileErr) {
      return next(new AppError("Invalid image format"));
    }
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Prduct not found", 404));
    }
    const imageURL = `${req.finalDestination}/${req.file.filename}`;
    const updatedProduct = await Product.findByIdAndUpdate(
      req.params.productId,
      { $set: { coverImage: imageURL } },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Cover image uploaded",
      data: updatedProduct,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Upload product's images
// @route   PATCH /api/v1/products/:productId/upload/images <==========> /api/v1/subcategories/:subcategoryId/products/:productId/upload/images
// @access  Private
const uploadProductsImages = async (req, res, next) => {
  try {
    if (req.fileErr) {
      return next(new AppError("Invalid image format"));
    }
    console.log('zzz', req.files);
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Prduct not found", 404));
    }
    let imagesURL = [];
    req.files.forEach((file) => {
      imagesURL.push(`${req.finalDestination}/${file.filename}`);
    });
    const updatedProduct = await Product.findByIdAndUpdate(
      req.params.productId,
      { $set: { images: imagesURL } },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Product images uploaded",
      data: updatedProduct,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Add product to user's whishlist
// @route   PATCH /api/v1/products/:productId/add <=====> /api/v1/subcategories/:subcategoryId/products/:producId/add
// @access  Private
const addToWhishlist = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Product not found", 404));
    }
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $push: { wishList: product._id } },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Product added to whishlist",
      data: updatedUser,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Remove product from user's whishlist
// @route   PATCH /api/v1/products/:productId/remove <=====> /api/v1/subcategories/:subcategoryId/products/:producId/remove
// @access  Private
const removeFromWhishlist = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Product not found", 404));
    }
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $pull: { wishList: product._id } },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Product removed to whishlist",
      data: updatedUser,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get random products
// @route   GET /api/v1/products/random <======> /api/v1/subcategories/:subcategoryId/products/random
// @access  Public
const getRandomProducts = async (req, res, next) => {
  try {
    const resultsPerPage = Number(process.env.RESULTS_PER_PAGE);
    const apiFeatures = new APIFeatures(
      Product.aggregate(Number(process.env.RANDOM_PRODUCTS_COUNT)),
      req.query
    ).pagination(resultsPerPage);
    const products = await apiFeatures.query;
    res.status(200).json({
      status: "Success",
      data: products,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Get best seller products
// @route   GET /api/v1/products/bestseller
// @access  Public
const getBestSellerProducts = async (req, res, next) => {
  try {
    const resultsPerPage = Number(process.env.RESULTS_PER_PAGE);
    const apiFeatures = new APIFeatures(
      Product.aggregate(Number(process.env.RANDOM_PRODUCTS_COUNT)),
      req.query
    ).pagination(resultsPerPage);
    const products = await apiFeatures.query;
    res.status(200).json({
      status: "Success",
      data: products,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Add product to user's cart
// @route   PATCH /api/v1/products/:productId/cart <=====> /api/v1/subcategories/:subcategoryId/products/:producId/cart
// @access  Private
const addToCart = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Product not found", 404));
    }
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $push: { cart: product._id } },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Product added to whishlist",
      data: updatedUser,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

// @desc    Remove product from user's cart
// @route   PATCH /api/v1/products/:productId/cart/remove <=====> /api/v1/subcategories/:subcategoryId/products/:producId/cart/remove
// @access  Private
const removeFromCart = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      return next(new AppError("Product not found", 404));
    }
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $pull: { cart: product._id } },
      { new: true }
    );
    res.status(200).json({
      status: "Success",
      message: "Product removed to whishlist",
      data: updatedUser,
    });
  } catch (error) {
    return next(new AppError(error.message, 500));
  }
};

module.exports = {
  createProduct,
  updateProduct,
  deleteProduct,
  getProduct,
  getAllProducts,
  getProductsByCategoryId,
  getProductsBySeller,
  uploadCoverImg,
  uploadProductsImages,
  addToWhishlist,
  removeFromWhishlist,
  getRandomProducts,
  addToCart,
  removeFromCart,
};
