const cloudinary = require('cloudinary').v2;

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_API_KEY,
    api_secret: process.env.CLOUD_API_SECRET
});

const destroyFile = (PublicID) =>
  cloudinary.uploader.destroy(PublicID, (err, des) => des);

const uploadFile = (file, folderName, width) =>
  cloudinary.uploader.upload(file, {
      folder: `${process.env.CLOUD_PROJECT}/${folderName}`,
      width: width,
      crop: 'fit',
  });

module.exports = {destroyFile, uploadFile}
