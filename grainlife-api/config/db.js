const mongoose = require("mongoose");

const connectDB = () => {
  const uri = `mongodb://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOST}:${process.env.MONGODB_DOCKER_PORT}/${process.env.MONGODB_DATABASE}`;
  return mongoose
    .connect(uri)
    .then(() => console.log("Database connected"))
    .catch((err) => console.log(err));
};

module.exports = connectDB;
