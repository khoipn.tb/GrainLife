const mongoose = require("mongoose");
const slugify = require("slugify");
const slug = require("mongoose-slug-generator");

const options = {
  separator: '-',
  lang: 'en',
  truncate: 120
};

mongoose.plugin(slug, options);

const productSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true },
    description: { type: String, required: true },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      required: true,
    },
    price: { type: Number, required: true },
    priceSale: Number,
    inStock: { type: Boolean, default: true },
    isActive: { type: Boolean, default: true },
    ratingsAverage: { type: Number, default: 0 },
    ratingsQuantity: Number,
    reviews: [{ type: mongoose.Schema.Types.ObjectId, ref: "Review" }],
    images: [String],
    imagesId: [String],
    slug: String,
  },
  { timestamps: true }
);

productSchema.pre("save", function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

productSchema.pre("updateOne", function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
