const mongoose = require("mongoose");
const slugify = require("slugify");
const slug = require('mongoose-slug-generator');

const options = {
    separator: '-',
    lang: 'en',
    truncate: 120
};

mongoose.plugin(slug, options);

const categorySchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    coverImage: String,
    description: { type: String },
    slug: { type: String, slug: 'name', unique: true },
    isActive: { type: Boolean, default: true }
  },
  { timestamps: true }
);

categorySchema.pre("save", function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

categorySchema.pre("update", function (next) {
    this.slug = slugify(this.name, { lower: true });
    next();
});

const Category = mongoose.model("Category", categorySchema);

module.exports = Category;
